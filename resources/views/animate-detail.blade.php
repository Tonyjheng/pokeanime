<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->
        <!-- jQuery UI --> 
        <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">

        <!-- Mobile menu -->
        <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
        <!-- Select -->
        <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" /> 
        <!-- Swiper slider -->
        <link href="{{asset('css/external/idangerous.swiper.css')}}" rel="stylesheet" />
        <!-- Magnific-popup -->
        <link href="{{asset('css/external/magnific-popup.css')}}" rel="stylesheet" />

        <!-- Custom -->
        <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

        <!-- Modernizr --> 
        <script src="{{asset('js/external/modernizr.custom.js')}}"></script>
@endsection

@section('searchbar')
    @include('layout.searchbar')
@endsection

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')
<div class="col-sm-12">
    <div class="movie">
        <h2 class="page-heading">{{ $Animate->name }} ( {{ $Animate->name_jp }} )</h2>
        
        <div class="movie__info">
            <div class="col-sm-4 col-md-3 movie-mobile">
                <div class="movie__images">
                    <span class="movie__rating">{{ $Animate->point }}</span>
                    <img alt='' src="{{ $Animate->pic_detail }}">
                </div>
                {{-- <div class="movie__rate">Your vote: <div id='score' class="score"></div></div> --}}
            </div>

            <div class="col-sm-8 col-md-9">
                <p class="movie__time">{{ $Animate->premiere }}</p>

                {{-- <p class="movie__option"><strong>Country: </strong><a href="#">New Zeland</a>, <a href="#">USA</a></p>
                <p class="movie__option"><strong>Actors: </strong><a href="#">Martin Freeman</a>, <a href="#">Ian McKellen</a>, <a href="#">Richard Armitage</a>, <a href="#">Ken Stott</a>, <a href="#">Graham McTavish</a>, <a href="#">Cate Blanchett</a>, <a href="#">Hugo Weaving</a>, <a href="#">Ian Holm</a>, <a href="#">Elijah Wood</a> <a href="#">...</a></p> --}}

                <p class="movie__option"><strong>原著作者: </strong><a href="javascript:;">{{ $Animate->author }}</a></p>
                <p class="movie__option"><strong>導演監督: </strong><a href="javascript:;">{{ $Animate->director }}</a></p>
                <p class="movie__option"><strong>製作廠商: </strong><a href="javascript:;">{{ $Animate->company }}</a></p>
                <p class="movie__option"><strong>作品平台: </strong><a href="javascript:;">{{ $Animate->mode }}</a></p>
                <p class="movie__option"><strong>播出集數: </strong><a href="javascript:;">{{ $Animate->episodes }}</a></p>
                <p class="movie__option"><strong>台灣代理: </strong><a href="javascript:;">{{ $Animate->proxy }}</a></p>
                <p class="movie__option"><strong>作品類型: </strong><a href="javascript:;">{{ $Animate->type }}</a></p>
                <p class="movie__option"><strong>作品分級: </strong><a href="javascript:;">{{ $Animate->level }}</a></p>
                <p class="movie__option"><strong>官方網站: </strong><a href="{{ $Animate->website }}" target="_blank">{{ $Animate->website }}</a></p>

                {{-- <a href="#" class="comment-link">Comments:  15</a> --}}

                <div class="movie__btns movie__btns--full">
                    {{-- <a href="#" class="btn btn-md btn--warning">book a ticket for this movie</a> --}}
                    {{-- <a href="#" class="watchlist">Add to watchlist</a> --}}
                </div>

            </div>
        </div>
        
        <div class="clearfix"></div>
        
        <h2 class="page-heading">動畫介紹</h2>

        <p class="movie__describe"> {!! nl2br($Animate->introduction) !!} </p>

        <di class="col-sm-4">{!! nl2br($Animate->staff) !!}</di>
        <div class="col-sm-4">{!! nl2br($Animate->cast) !!}</div>
        <p class="col-sm-4">
            @if($Animate->myreview!=null)
            <div class="col-sm-6 col-md-4">
                <div class="testionaial testionaial--rect">
                    <div class="testimonial-inner">
                        <div class="testionaial__text">{{$Animate->myreview}}</div>
                        <p class="testionaial__author">-小編無雷心得</p>
                    </div>
                </div>    
            </div>
            @endif
        </p>
        <div class="clearfix"></div>

        <h2 class="page-heading">線上收看 &amp; PV預告</h2>
        {{-- <h2 class="page-heading">photos &amp; videos</h2> --}}
        
        <div class="movie__media">
            {{-- <div class="movie__media-switch">
                <a href="#" class="watchlist list--photo" data-filter='media-photo'>234 photos</a>
                <a href="#" class="watchlist list--video" data-filter='media-video'>10 videos</a>
            </div> --}}

            <div class="swiper-container">
              <div class="swiper-wrapper">
                    {{-- <div class="swiper-slide">
                        @if($Animate->pv != null AND $Animate->pv != '')
                           <a href='{{$Animate->pv}}' target="_blank">
                        @else 
                            <a href='javascript:;'>
                        @endif
                            <img alt='' src="{{ asset('css/img/pv.jpg') }}">
                    </div> --}}
                    @if($Animate->pv != null AND $Animate->pv != '')
                    <div class="swiper-slide">
                        <a href='{{$Animate->pv}}' target="_blank">
                        <img alt='' src="{{ asset('css/img/pv.jpg') }}">
                    </div>
                    @endif

                    {{-- <div class="swiper-slide">
                        @if($Animate->gamer != null AND $Animate->gamer != '')
                            <a href='{{$Animate->gamer}}' target="_blank">
                        @else 
                            <a href='javascript:;'>
                        @endif
                            <img alt='' src="{{asset('css/img/gamer.jpg')}}">
                        </a>
                    </div> --}}
                    @if($Animate->gamer != null AND $Animate->gamer != '')
                    <div class="swiper-slide">
                        <a href='{{$Animate->gamer}}' target="_blank">
                            <img alt='' src="{{asset('css/img/gamer.jpg')}}">
                        </a>
                    </div>
                    @endif

                    {{-- <div class="swiper-slide">
                        @if($Animate->bilibili != null AND $Animate->bilibili != '')
                            <a href='{{$Animate->bilibili}}' target="_blank">
                        @else 
                            <a href='javascript:;'>
                        @endif
                            <img alt='' src="{{asset('css/img/bilibili.jpg')}}">
                        </a>
                    </div> --}}
                    @if($Animate->bilibili != null AND $Animate->bilibili != '')
                    <div class="swiper-slide">
                        <a href='{{$Animate->bilibili}}' target="_blank">
                            <img alt='' src="{{asset('css/img/bilibili.jpg')}}">
                        </a>
                    </div>
                    @endif

                    {{-- <div class="swiper-slide">
                        @if($Animate->iqiyi != null AND $Animate->iqiyi != '')
                            <a href='{{$Animate->iqiyi}}' target="_blank">
                        @else 
                            <a href='javascript:;'>
                        @endif
                            <img alt='' src="{{asset('css/img/iqiyi.jpg')}}">
                        </a>
                    </div> --}}
                    @if($Animate->iqiyi != null AND $Animate->iqiyi != '')
                    <div class="swiper-slide">
                        <a href='{{$Animate->iqiyi}}' target="_blank">
                            <img alt='' src="{{asset('css/img/iqiyi.jpg')}}">
                        </a>
                    </div>
                    @endif
  
                  <!--Second Slide-->
                  {{-- <div class="swiper-slide media-video">
                    <a href='https://www.youtube.com/watch?v=Kb3ykVYvT4U' class="movie__media-item">
                        <img alt='' src="http://placehold.it/400x240">
                    </a>
                  </div> --}}
                  
                  <!--Third Slide-->
                  {{-- <div class="swiper-slide media-photo"> 
                        <a href='{{asset('css/img/gamer.jpg')}}' class="movie__media-item">
                            <img alt='' src="http://placehold.it/400x240">
                         </a>
                  </div> --}}
            
              </div>
            </div>

        </div>

    </div>

</div>
@endsection

@section('bottomScript')
<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
        <!-- jQuery UI -->
        <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <!-- Bootstrap 3--> 
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

        <!-- Mobile menu -->
        <script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
         <!-- Select -->
        <script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script>

        <!-- Stars rate -->
        <script src="{{asset('js/external/jquery.raty.js')}}"></script>
        <!-- Swiper slider -->
        <script src="{{asset('js/external/idangerous.swiper.min.js')}}"></script>
        <!-- Magnific-popup -->
        <script src="{{asset('js/external/jquery.magnific-popup.min.js')}}"></script> 

        <!--*** Google map  ***-->
        {{-- <script src="https://maps.google.com/maps/api/js?sensor=true"></script>  --}}
        <!--*** Google map infobox  ***-->
        {{-- <script src="{{asset('js/external/infobox.js')}}"></script>  --}}

        <!-- Form element -->
        <script src="{{asset('js/external/form-element.js')}}"></script>
        <!-- Form validation -->
        <script src="{{asset('js/form.js')}}"></script>

        <!-- Custom -->
        <script src="{{asset('js/custom.js')}}"></script>
		
		<script type="text/javascript">
            $(document).ready(function() {
                init_MoviePage();
                init_MoviePageFull();
            });
		</script>
@endsection
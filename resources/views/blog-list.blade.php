<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
<!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script> 
@endsection

@section('bottomScript')
<!-- JavaScript-->
    <!-- jQuery 1.9.1--> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
    <!-- Migrate --> 
    <script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
    <!-- Bootstrap 3--> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

    <!-- Mobile menu -->
    <script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
    <!-- Select -->
    <script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script>

    <!-- Twitter feed -->
    {{-- <script src="{{asset('js/external/twitterfeed.js')}}"></script> --}}
    
    <!-- Form element -->
    <script src="{{asset('js/external/form-element.js')}}"></script>
    <!-- Form validation -->
    <script src="{{asset('js/form.js')}}"></script>

    <!-- Custom -->
    <script src="{{asset('js/custom.js')}}"></script>
    
@endsection

@section('searchbar')
    @include('layout.blogSearchbar')
@endsection

@section('content')
<!-- Main content -->
<section class="container">
    <div class="overflow-wrapper">
        <div class="col-sm-8 col-md-9">
        
            <h2 class="page-heading">{{$title}}</h2>
            
            @foreach ($Blogs as $blog)
            <!-- News post article-->
            <article class="post post--news">
                    <a href='/blog/{{$blog->id}}' class="post__image-link">
                        <img alt='' src="{{$blog->cover}}">
                    </a>
    
                    <h1><a href="/blog/{{$blog->id}}" class="post__title-link">{{$blog->title}}</a></h1>
                    <p class="post__date">{{$blog->postDate}} </p>
    
                    <div class="wave-devider"></div>
    
                    <p class="post__text">{{$blog->listIntro}}</p> 
    
                    {{-- <div class="tags">
                            <ul>
                                <li class="item-wrap"><a href="#" class="tags__item">news</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item">movie</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item">ceremony</a></li>
                                <li class="item-wrap"><a href="#" class="tags__item">award</a></li>
                            </ul>
                    </div> --}}
    
                    <div class="devider-huge"></div>
                </article> 
                <!-- end news post article-->
            @endforeach

            <div class="pagination">
                @if($Blogs->previousPageUrl() != null)
                <a href='{{$Blogs->previousPageUrl()}}' class="pagination__prev">上一頁</a>
                @endif
    
                @if($Blogs->nextPageUrl() != null)
                <a href='{{$Blogs->nextPageUrl()}}' class="pagination__next">下一頁</a>
                @endif
            </div>

        </div>

        <aside class="col-sm-4 col-md-3">
            <div class="sitebar">
                <div class="category category--popular marginb-sm">
                    <h3 class="category__title">精選 <br><span class="title-edition">熱門文章</span></h3>
                    <ul>
                        @foreach ($BlogsPop as $BlogsP)
                        <li><a href="/blog/{{$BlogsP->id}}" class="category__item">{{$BlogsP->title}}</a></li>                            
                        @endforeach
                    </ul>
                </div>

                <div class="banner-wrap">
                    <img alt='banner' src="{{asset('images/banners/sidebanner-01.jpg')}}">
                </div>

                {{-- <div class="category category--discuss marginb-sm">
                    <h3 class="category__title">the Most <br><span class="title-edition">discussed posts</span></h3>
                    <ul>
                        <li><a href="#" class="category__item">The best movie to see this month</a></li>
                        <li><a href="#" class="category__item">Look forward to films such as 'Saving Mr Banks', 'The Book Thief' and 'Robocop'</a></li>
                        <li><a href="#" class="category__item">The 100 best romantic movies</a></li>
                        <li><a href="#" class="category__item">The 50 greatest animated films</a></li>
                        <li><a href="#" class="category__item">Tickets to Taste of Christmas</a></li>
                        <li><a href="#" class="category__item">10 best film pop-ups this week</a></li>
                        <li><a href="#" class="category__item">How to take a part in ‘Star Wars’</a></li>
                    </ul>
                </div>
                
                <div class="banner-wrap">
                    <img alt='banner' src="http://placehold.it/500x500">
                </div> --}}

                {{-- <div class="category category--light categoty--position marginb-sm">
                    <h3 class="category__title">browse<br><span class="title-edition">by section</span></h3>
                    <ul>
                        <li><a href="#" class="category__item">Showtimes &amp; Tickets (12345)</a></li>
                        <li><a href="#" class="category__item">Latest Trailers (3153)</a></li>
                        <li><a href="#" class="category__item">Coming Soon (3153)</a></li>
                        <li><a href="#" class="category__item">In Theaters (31)</a></li>
                        <li><a href="#" class="category__item">Release Calendar (314)</a></li>
                        <li><a href="#" class="category__item">Movies (654)</a></li>
                        <li><a href="#" class="category__item">Stars (241)</a></li>
                        <li><a href="#" class="category__item">In Theaters (8423)</a></li>
                        <li><a href="#" class="category__item">Release Calendar (246)</a></li>
                        <li><a href="#" class="category__item">Movies (18)</a></li>
                        <li><a href="#" class="category__item">Stars (471)</a></li>
                    </ul>
                </div> --}}

                {{-- <h3 class="heading-special lower--hight">tags</h3>
                <ul class="tags tags--dark">
                    <li class="item-wrap"><a href="#" class="tags__item">lorem ipsum </a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">consectetur</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">gravida</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">adipiscing</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">nunc ante</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">dapibus nec</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">commodo nec</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">quis justo</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">lorem ipsum </a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">consectetur</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">adipiscing</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">nunc ante</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">dapibus nec</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">commodo nec</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item">quis justo</a></li>
                </ul> --}}
  
                {{-- <h3 class="heading-special">latest<br><span class="heading-edition">tweets</span></h3>
                <div id="twitter-feed" class="twitter--large"></div> --}}

            </div>
        </aside>
    </div>
</section>
@endsection
<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')

    <!-- Page-Title -->
    @include('ubold.setting.animateSetting')
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                {{-- 錯誤訊息模板元件 --}}
                @include('ubold.components.validationErrorMessage')

                <div class="row">
                    <div class="col-md-6">
                        <form class="form-horizontal" role="form" method="post" action="/ubold/{{$Subtitles->id}}"> 
                            {{-- 隱藏方法欄位 --}}
                            {{ method_field('PUT') }}  

                            <div class="form-group">
                                <label class="col-md-2 control-label">字幕組名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="subtitles_name" name="subtitles_name" placeholder="請輸入字幕組名稱" value="{{ old('name' , $Subtitles->subtitles_name) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sort">排序</label>
                                <div class="col-md-10">
                                    <input type="text" id="sort" name="sort" class="form-control" placeholder="排序" value="{{ old('sort' , $Subtitles->sort)}}">
                                </div>
                            </div>                                                                                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">備註</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" id="remark" name="remark">{{ old('remark' , $Subtitles->remark ) }}</textarea>
                                </div>
                            </div>
                            
                            <div class="text-center p-20">
                                <button type="button" class="btn w-sm btn-white waves-effect" id="cnacelBtn">回列表</button>
                                <button type="submit" class="btn w-sm btn-default waves-effect waves-light" id="sendBtn">送出</button>
                            </div>

                            {{-- CSRF 欄位--}}
                            {{ csrf_field() }}
                        </form>
                    </div>
                    
                    <div class="col-md-6">
                        {{-- <form class="form-horizontal" role="form">                                    
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">Readonly</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" readonly="" value="Readonly value">
                                </div>
                            </div>                                    
                            <div class="form-group">
                                <label class="col-md-2 control-label">Disabled</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" disabled="" value="Disabled value">
                                </div>
                            </div>                                    
                            
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Static control</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static">email@example.com</p>
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Helping text</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Helping text">
                                    <span class="help-block"><small>A block of help text that breaks onto a new line and may extend beyond one line.</small></span>
                                </div>
                            </div>  

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Input Select</label>
                                <div class="col-sm-10">
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                    <h6>Multiple select</h6>
                                    <select multiple="" class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
            
                        </form> --}}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection


@section('headScript')
<script src="{{url('js/sweetalert2.all.min.js')}}"></script>
@endsection


@section('bodyScript')
<script>
    $(document).ready(function () {
        $("#cnacelBtn").on("click", function() {
            document.location.href=('../subtitles')
        });
    });
</script>
@endSection
<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

@section('headScript')
<link href="{{url('/ubold/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<meta name="_token" content="{{ csrf_token() }}"/>

{{-- switch --}}
<link href="{{url('/ubold/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" />

{{-- ckeditor --}}
<script src="{{asset('class/ckeditor/ckeditor.js')}}"></script>
@endSection

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')
        
        <!-- Page-Title -->
        @include('ubold.setting.animateSetting')

        <div class="row">
            <div class="col-sm-12">
                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="/ubold/blog/{{$Blog->id}}">          
                    <div class="card-box">
                        {{-- 錯誤訊息模板元件 --}}
                        @include('ubold.components.validationErrorMessage')
        
                        <div class="row">
                            <div class="col-md-6">
                                {{-- 隱藏方法欄位 --}}
                                {{ method_field('PUT') }}  

                                <div class="form-group">
                                    <label class="col-md-2 control-label">文章標題</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control"  id="title" name="title" placeholder="請輸入標題" value="{{ old('title' , $Blog->title) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sort">排序</label>
                                    <div class="col-md-10">
                                        <input type="text" id="sort" name="sort" class="form-control" placeholder="排序" value="{{ old('sort' , $Blog->sort) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sort">點擊數</label>
                                    <div class="col-md-10">
                                        <input type="text" id="hits" name="hits" class="form-control" placeholder="點擊數" value="{{ old('hits' , $Blog->hits) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="sort">發佈時間</label>
                                    <div class="col-md-10">
                                        <input type="text" id="postDate" name="postDate" class="form-control" placeholder="發佈時間" value="{{ old('postDate' , $Blog->postDate) }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">是否顯示</label>
                                    <div class="col-md-10">
                                        <input type="checkbox" name="show" data-plugin="switchery" @if(old('show', $Blog->show)=='1') checked @endif data-color="#5d9cec"/>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-md-2 control-label">封面圖片</label>
                                    <div class="col-md-10">
                                        <input type="file" class="filestyle" data-placeholder="No file" name="cover">
                                    </div>
                                    {{-- <div class="col-md-2">
                                        <img src="{{$Blog->cover or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive thumb-md img-rounded">
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <img src="{{$Blog->cover or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive img-rounded">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">首頁封面圖片</label>
                                    <div class="col-md-10">
                                        <input type="file" class="filestyle" data-placeholder="No file" name="cover_index">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-10">
                                        <img src="{{$Blog->cover_index or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive img-rounded">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">列表簡介</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" rows="5" name="listIntro">{{ old('listIntro' , $Blog->listIntro) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">文章內容</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" rows="5" name="content" id="content">{{ old('content' , $Blog->content) }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">備註</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" rows="5" id="remark" name="remark">{{ old('remark' , $Blog->remark) }}</textarea>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="text-center p-20">
                            <button type="button" class="btn w-sm btn-white waves-effect" id="cnacelBtn">回列表</button>
                            <button type="submit" class="btn w-sm btn-default waves-effect waves-light" id="sendBtn">送出</button>
                        </div>
                        
                    </div>

                    {{-- CSRF 欄位--}}
                    {{ csrf_field() }}
                </form>
            </div>
        </div>

@endsection

@section('bodyScript')
{{-- switch --}}
<script src="{{url('/ubold/assets/plugins/switchery/js/switchery.min.js')}}"></script>

{{-- file upload --}}
<script src="{{url('/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>

<script>
    $(document).ready(function () {
        CKEDITOR.replace( 'content' );
        
        $("#cnacelBtn").on("click", function() {
            location.href=('/ubold/blog')
        });

        $("button#delBtn").on("click", function() {
            id = $(this).attr("data-id");
            // id = '10';
            // swal(id);
            swal({
                title: '確定要刪除嗎?',
                text: "刪除就瑞凡了喔!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '取消'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "delete",
                        url: "animate/animateDel",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "id="+id,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '刪除成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/animate')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '刪除失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else if (msg=='002') {
                                swal({
                                    title: '失敗',
                                    text: '刪除資料夾失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else {
                                swal({
                                    title: '失敗',
                                    // text: '刪除失敗',
                                    text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                            // swal(msg);
                        }
                    });
                }
            })
        });
    });
</script>
@endSection
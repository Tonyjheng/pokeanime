<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">設定 <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
            <ul class="dropdown-menu drop-menu-right" role="menu">
                <li><a href="/ubold/users/userAdd">新增使用者</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </div>

        <h4 class="page-title">{{ $title }}</h4>
        <ol class="breadcrumb">
            <li>
                <a href="#">Ubold</a>
            </li>
            <li>
                <a href="#">會員區</a>
            </li>
            <li class="active">
                {{ $title }}
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">設定 <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
            <ul class="dropdown-menu drop-menu-right" role="menu">
                <li><a href="/ubold/subtitlesAdd">新增字幕組</a></li>
                <li><a href="/ubold/animate/animateAdd">新增動畫</a></li>
                <li><a href="/ubold/blog/blogAdd">新增文章</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
            </ul>
        </div>

        <h4 class="page-title">{{ $title }}</h4>
        <ol class="breadcrumb">
            <li>
                <a href="#">Ubold</a>
            </li>
            <li>
                <a href="#">動畫區</a>
            </li>
            <li class="active">
                {{ $title }}
            </li>
        </ol>
    </div>
</div>
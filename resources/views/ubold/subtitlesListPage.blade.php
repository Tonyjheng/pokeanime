<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

@section('headScript')
<!-- DataTables -->
<link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

<script src="{{url('js/sweetalert2.all.min.js')}}"></script>
<meta name="_token" content="{{ csrf_token() }}"/>
@endSection

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')
        {{-- <h1>{{ $title }}</h1> --}}
        {{-- <table>
        @foreach($SubtitlesPaginate as $Subtitle)
            <tr>
                <td> {{ $Subtitle->subtitles_name }}</td>
                <td> {{ $Subtitle->sort }}</td>
            </tr>
        @endforeach
        </table> --}}
        {{-- 分頁頁數按鈕 --}}
        {{-- <BR>{{ $SubtitlesPaginate->links() }} --}}


        <!-- Page-Title -->
        @include('ubold.setting.animateSetting')

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">

                    {{-- <h4 class="m-t-0 header-title"><b>Responsive example</b></h4>
                    <p class="text-muted font-13 m-b-30">
                        Responsive is an extension for DataTables that resolves that problem by optimising the
                        table's layout for different screen sizes through the dynamic insertion and removal of
                        columns from the table.
                    </p> --}}

                    <table id="datatable-responsive"
                            class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                            width="100%" action="/ubold/subtitlesAdd" method="post">
                        {{-- 隱藏方法欄位 --}}
                        {{ method_field('PUT') }}
                        
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>字幕組名稱</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($SubtitlesPaginate as $Subtitle)
                                <tr>
                                    <td> {{ $Subtitle->id }}</td>
                                    <td> {{ $Subtitle->subtitles_name }}</td>
                                    <td> 
                                        <a class="btn btn-icon waves-effect waves-light btn-primary" href="{{$Subtitle->id}}/edit"> <i class="fa fa-wrench"></i> </a>
                                        <button class="btn btn-icon waves-effect waves-light btn-danger" id="delBtn" data-id="{{$Subtitle->id}}"> <i class="fa fa-remove"></i> </button>
                                        {{-- <button class="btn btn-default  waves-effect waves-light btn-sm">Small button</button>
                                        <button class="btn btn-danger waves-effect waves-light btn-sm">Small button</button> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

@endsection

@section('bodyScript')
<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>

<script>
    $(document).ready(function () {
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "assets/plugins/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $("button#delBtn").on("click", function() {
            id = $(this).attr("data-id");
            // id = '10';
            // swal(id);
            swal({
                title: '確定要刪除嗎?',
                text: "刪除就瑞凡了喔!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '取消'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "delete",
                        url: "subtitlesdel",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "id="+id,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '刪除成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/subtitles')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '刪除失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else {
                                swal({
                                    title: '失敗',
                                    text: '刪除失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                        }
                    });
                }
            })
        });
    });
</script>
@endSection
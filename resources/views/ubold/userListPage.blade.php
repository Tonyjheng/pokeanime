<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

@section('headScript')
<!-- DataTables -->
<link href="assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

<script src="{{url('js/sweetalert2.all.min.js')}}"></script>
<meta name="_token" content="{{ csrf_token() }}"/>
@endSection

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')
        
        <!-- Page-Title -->
        @include('ubold.setting.usersSetting')

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">

                    <table id="datatable-responsive"
                            class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                            width="100%">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>姓名</th>
                            <th>信箱</th>
                            <th>狀態</th>
                            <th>動作</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($Users as $User)
                                <tr>
                                    <td> {{ $User->id }} </td>
                                    <td> {{ $User->name }}({{$User->nickname}}) </td>
                                    <td> {{ $User->email }} </td>
                                    <td>
                                        @if ($User->status =='0')
                                            <button type="button" class="btn btn-success waves-effect waves-light">正常登入</button>
                                        @else
                                            <button type="button" class="btn btn-danger waves-effect waves-light">禁止登入</button>
                                        @endif

                                        @if ($User->type =='A')
                                            <button type="button" class="btn btn-purple waves-effect waves-light">管理員</button>
                                        @else
                                            <button type="button" class="btn btn-default waves-effect waves-light">一般會員</button>
                                        @endif
                                    </td>
                                    <td> 
                                        <a class="btn btn-icon waves-effect waves-light btn-primary" href="/ubold/users/{{$User->id}}/edit"> <i class="fa fa-wrench"></i> </a>
                                        <button class="btn btn-icon waves-effect waves-light btn-warning" id="sealBtn" data-id="{{$User->id}}"> <i class="fa fa-inbox"></i> </button>
                                        <button class="btn btn-icon waves-effect waves-light btn-danger" id="delBtn" data-id="{{$User->id}}"> <i class="fa fa-remove"></i> </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
    
                        <h4 class="m-t-0 header-title"><b>已封存的會員</b></h4>
                        <table id="datatable-responsive2"
                                class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>姓名</th>
                                <th>信箱</th>
                                <th>狀態</th>
                                <th>動作</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($UsersSeals as $UsersSeal)
                                    <tr>
                                        <td> {{ $UsersSeal->id }} </td>
                                        <td> {{ $UsersSeal->name }}({{$UsersSeal->nickname}}) </td>
                                        <td> {{ $UsersSeal->email }} </td>
                                        <td>
                                            @if ($UsersSeal->status =='0')
                                                <button type="button" class="btn btn-success waves-effect waves-light">正常登入</button>
                                            @else
                                                <button type="button" class="btn btn-danger waves-effect waves-light">禁止登入</button>
                                            @endif
    
                                            @if ($UsersSeal->type =='A')
                                                <button type="button" class="btn btn-purple waves-effect waves-light">管理員</button>
                                            @else
                                                <button type="button" class="btn btn-default waves-effect waves-light">一般會員</button>
                                            @endif
                                        </td>
                                        <td> 
                                            <a class="btn btn-icon waves-effect waves-light btn-primary" href="/ubold/users/{{$UsersSeal->id}}/edit"> <i class="fa fa-wrench"></i> </a>
                                            <button class="btn btn-icon waves-effect waves-light btn-warning" id="sealCancelBtn" data-id="{{$UsersSeal->id}}"> <i class="fa fa-inbox"></i> </button>
                                            <button class="btn btn-icon waves-effect waves-light btn-danger" id="delBtn" data-id="{{$UsersSeal->id}}"> <i class="fa fa-remove"></i> </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
    
                    </div>
                </div>
            </div>
        
@endsection

@section('bodyScript')
<!-- DataTables -->
<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/jszip.min.js"></script>
<script src="assets/plugins/datatables/pdfmake.min.js"></script>
<script src="assets/plugins/datatables/vfs_fonts.js"></script>
<script src="assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="assets/plugins/datatables/buttons.print.min.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="assets/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="assets/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script src="assets/plugins/datatables/dataTables.colVis.js"></script>
<script src="assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>

<script>
    $(document).ready(function () {
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
            ajax: "assets/plugins/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $('#datatable-responsive2').DataTable();
        $('#datatable-scroller2').DataTable({
            ajax: "assets/plugins/datatables/json/scroller-demo.json",
            deferRender: true,
            scrollY: 380,
            scrollCollapse: true,
            scroller: true
        });

        $("button#delBtn").on("click", function() {
            id = $(this).attr("data-id");
            // id = '10';
            // swal(id);
            swal({
                title: '確定要刪除嗎?',
                text: "資料將會刪除!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '取消'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "delete",
                        url: "users/userDel",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "id="+id,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '刪除成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/users')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '刪除失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else {
                                swal({
                                    title: '失敗',
                                    // text: '刪除失敗',
                                    text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                        }
                    });
                }
            })
        });

        //封存會員
        $("button#sealBtn").on("click", function() {
            id = $(this).attr("data-id");
            // id = '10';
            // swal(id);
            swal({
                title: '確定要封存嗎?',
                text: "封存不會刪除任何資料",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '取消'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "delete",
                        url: "users/userSeal",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "id="+id,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '封存成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/users')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '封存失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else {
                                swal({
                                    title: '失敗',
                                    // text: '刪除失敗',
                                    text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                            // swal(msg);
                        }
                    });
                }
            })
        });

        $("button#sealCancelBtn").on("click", function() {
            id = $(this).attr("data-id");
            // swal(id);
            swal({
                title: '確定要取消封存嗎?',
                text: "取消封存不會刪除任何資料",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是',
                cancelButtonText: '取消'
                }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "post",
                        url: "users/userSealCancel",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "id="+id,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '取消封存成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/users')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '取消封存失敗',
                                    // text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            } else {
                                swal({
                                    title: '失敗',
                                    // text: '刪除失敗',
                                    text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                            // swal(msg);
                            // console.log(msg);
                        }
                    });
                }
            })
        });

    });
</script>
@endSection
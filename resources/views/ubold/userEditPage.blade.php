<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')

    <!-- Page-Title -->
    @include('ubold.setting.usersSetting')
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                {{-- 錯誤訊息模板元件 --}}
                @include('ubold.components.validationErrorMessage')

                <form class="form-horizontal" role="form" method="post" action="/ubold/users/{{ $User->id }}"> 
                
                    {{-- 隱藏方法欄位 --}}
                    {{ method_field('PUT') }}  

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">姓名</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="name" name="name" placeholder="請輸入姓名" value="{{ old('name', $User->name) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">暱稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="nickname" name="nickname" placeholder="請輸入暱稱" value="{{ old('nickname', $User->nickname) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">信箱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="email" name="email" placeholder="請輸入信箱" value="{{ old('email', $User->email) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">備註</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" id="remark" name="remark">{{ old('remark', $User->remark) }}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-2 control-label">權限等級</label>
                                <div class="col-md-10">
                                    <div class="radio radio-success radio-inline">
                                        <input type="radio" id="statusInlineRadio1" name="type" value="A" @if(old('type', $User->type)=='A') checked @endif>
                                        <label for="statusInlineRadio1"> 管理員 </label>
                                    </div>
                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" id="statusInlineRadio2" name="type" value="G" @if(old('type', $User->type)=='G') checked @endif>
                                        <label for="statusInlineRadio2"> 一般會員 </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">是否封存</label>
                                <div class="col-md-10">
                                    <input type="checkbox" name="seal" @if(old('seal', $User->seal)=='1') checked @endif data-plugin="switchery" data-color="#5d9cec"/>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label">禁止登入</label>
                                <div class="col-md-10">
                                    <input type="checkbox" name="status" @if(old('status', $User->status)=='1') checked @endif data-plugin="switchery" data-color="#5d9cec"/>
                                </div>
                            </div> 

                            <hr>

                            <div class="form-group">
                                <label class="col-md-2 control-label" for="password">密碼</label>
                                <div class="col-md-10">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="請輸入密碼" value="{{ old('password') }}">
                                    <span class="help-block"><small class="text-danger">如果需要更改密碼再填寫</small></span>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="password_confirmation">確認密碼</label>
                                <div class="col-md-10">
                                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" placeholder="請輸入密碼" value="{{ old('password_confirmation') }}">
                                </div>
                            </div>           
                            
                        </div>
                    </div>

                    <div class="text-center p-20">
                        <button type="button" class="btn w-sm btn-white waves-effect" id="cnacelBtn">回列表</button>
                        <button type="submit" class="btn w-sm btn-default waves-effect waves-light" id="sendBtn">送出</button>
                    </div>

                    {{-- CSRF 欄位--}}
                    {{ csrf_field() }}
                </form>

            </div>
        </div>
    </div>
@endsection


@section('headScript')
<script src="{{url('js/sweetalert2.all.min.js')}}"></script>

{{-- switch --}}
<link href="{{url('/ubold/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" />
<link href="{{url('/ubold/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('bodyScript')

{{-- switch --}}
<script src="{{url('/ubold/assets/plugins/switchery/js/switchery.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $("#cnacelBtn").on("click", function() {
            location.href=('/ubold/users')
        });
    });
</script>
@endSection
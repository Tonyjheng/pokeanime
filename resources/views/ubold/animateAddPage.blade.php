<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

{{-- 改成新增完直接編輯的方式 --}}

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')

    <!-- Page-Title -->
    @include('ubold.setting.animateSetting')
    
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                {{-- 錯誤訊息模板元件 --}}
                @include('ubold.components.validationErrorMessage')

                <div class="row">
                    <div class="col-md-6">
                        <form class="form-horizontal" role="form" method="post" action="subtitlesAdd">                                    
                            <div class="form-group">
                                <label class="col-md-2 control-label">字幕組名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="subtitles_name" name="subtitles_name" placeholder="請輸入字幕組名稱" value="{{ old('subtitles_name') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="sort">排序</label>
                                <div class="col-md-10">
                                    <input type="text" id="sort" name="sort" class="form-control" placeholder="排序" value="{{ old('sort') }}">
                                </div>
                            </div>                                                                                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">備註</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" id="remark" name="remark">{{ old('remark') }}</textarea>
                                </div>
                            </div>
                            
                            <div class="text-center p-20">
                                <button type="button" class="btn w-sm btn-white waves-effect" id="cnacelBtn">回列表</button>
                                <button type="submit" class="btn w-sm btn-default waves-effect waves-light" id="sendBtn">送出</button>
                            </div>

                            {{-- CSRF 欄位--}}
                            {{ csrf_field() }}
                        </form>
                    </div>
                    
                    <div class="col-md-6"></div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('headScript')
<script src="{{url('js/sweetalert2.all.min.js')}}"></script>
{{-- <meta name="_token" content="{{ csrf_token() }}"/> --}}
@endsection

@section('bodyScript')
<script>
    $(document).ready(function () {
        $("#cnacelBtn").on("click", function() {
            location.href=('subtitles')
        });
    });
</script>
@endSection
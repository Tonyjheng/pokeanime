<!-- 指定繼承 ubold.page-starter 母模板 -->
@extends('ubold.page-starter')

<!-- 傳送資料到母模板，並指定變數為 title -->
@section('title', $title)

@section('headScript')
<script src="{{url('js/sweetalert2.all.min.js')}}"></script>

<!-- ION Slider -->
<link href="{{url('/ubold/assets/plugins/ion-rangeslider/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{url('/ubold/assets/plugins/ion-rangeslider/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet" type="text/css"/>

<link href="{{url('/ubold/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">

{{-- switch --}}
<link href="{{url('/ubold/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" />
<link href="{{url('/ubold/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

@endsection


@section('bodyScript')
<!-- ION Slider -->
<script src="{{url('/ubold/assets/plugins/ion-rangeslider/ion.rangeSlider.min.js')}}"></script>
<script src="{{url('/ubold/assets/plugins/bootstrap-slider/js/bootstrap-slider.min.js')}}"></script>

<script src="{{url('/ubold/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>

{{-- switch --}}
<script src="{{url('/ubold/assets/plugins/switchery/js/switchery.min.js')}}"></script>

{{-- file upload --}}
<script src="{{url('/ubold/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}" type="text/javascript"></script>



<script>
    $(document).ready(function () {
        point = $("#point").val();
        $("#pointBar").ionRangeSlider({
            min: 1,
            max: 5,
            from: point
        });

        jQuery('#datepicker-autoclose').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
        });

        $("#cnacelBtn").on("click", function() {
            document.location.href=('/ubold/animate')
        });

    });
</script>
@endSection

<!-- 傳送資料到母模板，並指定變數為 content -->
@section('content')

    <!-- Page-Title -->
    @include('ubold.setting.animateSetting')

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                {{-- 錯誤訊息模板元件 --}}
                @include('ubold.components.validationErrorMessage')

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="/ubold/animate/{{$Animate->id}}"> 
                    <div class="row">
                        <div class="col-md-6">
                            {{-- 隱藏方法欄位 --}}
                            {{ method_field('PUT') }}  

                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫狀態</label>
                                <div class="col-md-10">
                                    <div class="radio radio-success radio-inline">
                                        <input type="radio" id="statusInlineRadio1" value="P" name="status" @if(old('status', $Animate->status)=='P') checked @endif>
                                        <label for="statusInlineRadio1"> 連載中 </label>
                                    </div>
                                    <div class="radio radio-default radio-inline">
                                        <input type="radio" id="statusInlineRadio2" value="F" name="status" @if(old('status', $Animate->status)=='F') checked @endif>
                                        <label for="statusInlineRadio2"> 已完結 </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="name" name="name" placeholder="請輸入名稱" value="{{ old('name' , $Animate->name) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫日文名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="name_jp" name="name_jp" placeholder="請輸入日文名稱" value="{{ old('name_jp' , $Animate->name_jp) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">觀看狀態</label>
                                <div class="col-md-10">
                                    {{-- <input type="text" class="form-control"  id="read" name="read" placeholder="觀看狀態" value="{{ old('read' , $Animate->read) }}"> --}}
                                    <div class="radio radio-pink radio-inline">
                                        <input type="radio" id="readInlineRadio1" value="R" name="read" @if(old('read', $Animate->read)=='R') checked @endif>
                                        <label for="readInlineRadio1"> 觀看中 </label>
                                    </div>
                                    <div class="radio radio-primary radio-inline">
                                        <input type="radio" id="readInlineRadio2" value="F" name="read" @if(old('read', $Animate->read)=='F') checked @endif>
                                        <label for="readInlineRadio2"> 已看完 </label>
                                    </div>
                                    <div class="radio radio-default radio-inline">
                                        <input type="radio" id="readInlineRadio3" value="G" name="read" @if(old('read', $Animate->read)=='G') checked @endif>
                                        <label for="readInlineRadio3"> 放棄治療 </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫簡介</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="introduction" id="introduction" rows="5">{{ old('introduction' , $Animate->introduction) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">我的短評</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="myreview" id="myreview" rows="5">{{ old('myreview' , $Animate->myreview) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pointBar" class="col-sm-2 control-label">我的評分</label>
                                <div class="col-sm-10">
                                    <input type="text" id="pointBar" name="point">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">作品類型</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="type">
                                        @foreach($typeArray as $type)
                                            <option value="{{ $type }}" @if($type==$Animate->type) selected @endif>{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">播映方式</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="mode">
                                        <option value="動畫" @if(old('mode', $Animate->mode)=='動畫') selected @endif>動畫</option>
                                        <option value="電影" @if(old('mode', $Animate->mode)=='電影') selected @endif>電影</option>
                                    </select>
                                </div>
                            </div>   
                            <div class="form-group">
                                <label class="col-sm-2 control-label">當地首播</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="年-月-日" id="datepicker-autoclose" name="premiere" value="{{ old('premiere' , $Animate->premiere) }}">
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                    </div><!-- input-group -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">播出集數</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="episodes" name="episodes" placeholder="播出集數" value="{{ old('episodes' , $Animate->episodes) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">原著作者</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="author" name="author" placeholder="原著作者" value="{{ old('author' , $Animate->author) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">導演監督</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="director" name="director" placeholder="導演監督" value="{{ old('director' , $Animate->director) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製作廠商</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="company" name="company" placeholder="製作廠商" value="{{ old('company' , $Animate->company) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">台灣代理</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="proxy" name="proxy" placeholder="台灣代理" value="{{ old('proxy' , $Animate->proxy) }}">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label">官方網站</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="website" name="website" placeholder="官方網站" value="{{ old('website' , $Animate->website) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫PV</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="pv" name="pv" placeholder="動畫PV" value="{{ old('pv' , $Animate->pv) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫分級</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="level" name="level" placeholder="動畫分級" value="{{ old('level' , $Animate->level) }}">
                                </div>
                            </div>  
                            <div class="form-group">
                                <label class="col-md-2 control-label">聲優</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="cast" id="cast" rows="5">{{ old('cast' , $Animate->cast) }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製作團隊</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="staff" id="staff" rows="5">{{ old('staff' , $Animate->staff) }}</textarea>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label">動畫瘋</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="gamer" name="gamer" placeholder="巴哈姆特動畫瘋" value="{{ old('gamer' , $Animate->gamer) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">bilibili</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="bilibili" name="bilibili" placeholder="bilibili" value="{{ old('bilibili' , $Animate->bilibili) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">愛奇藝</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="iqiyi" name="iqiyi" placeholder="iqiyi" value="{{ old('iqiyi' , $Animate->iqiyi) }}">
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label">備註</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" name="remark" id="remark" rows="5">{{ old('remark' , $Animate->remark) }}</textarea>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-md-2 control-label">點擊數</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="hits" name="hits" placeholder="hits" value="{{ old('hits' , $Animate->hits) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">排序</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="sort" name="sort" placeholder="sort" value="{{ old('sort' , $Animate->sort) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">是否顯示</label>
                                <div class="col-md-10">
                                    <input type="checkbox" name="show" @if(old('show', $Animate->show)=='1') checked value="1" @endif data-plugin="switchery" data-color="#5d9cec"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">是否為新番</label>
                                <div class="col-md-10">
                                    <input type="checkbox" name="shinban" @if(old('shinban', $Animate->shinban)=='1') checked value="1" @endif data-plugin="switchery" data-color="#5d9cec"/>
                                </div>
                            </div>  
                        </div>
                    
                        <div class="col-md-6">
                            <h4 class="m-t-0 header-title text-purple"><b>個人使用區</b></h4>
                            <div class="form-group">
                                <label class="col-md-2 control-label">檔案大小</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="filesize" name="filesize" placeholder="檔案大小" value="{{ old('filesize' , $Animate->filesize) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">字幕組</label>
                                <div class="col-md-10">
                                    {{-- <input type="text" class="form-control"  id="subtitle" name="subtitle" placeholder="字幕組" value="{{ old('subtitle' , $Animate->subtitle) }}"> --}}
                                    <select class="form-control" name="subtitle">
                                        @foreach($Animate_subtitles as $Animate_subtitle)
                                            <option value="{{ $Animate_subtitle->id }}" @if($Animate->subtitle==$Animate_subtitle->id) selected @endif>{{ $Animate_subtitle->subtitles_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">存放位置</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control"  id="storage" name="storage" placeholder="存放位置" value="{{ old('storage' , $Animate->storage) }}">
                                </div>
                            </div>

                            <h4 class="m-t-0 header-title text-purple"><b>圖片區</b></h4>
                            <div class="form-group">
                                <label class="col-md-2 control-label">首頁上方釘選</label>
                                <div class="col-md-10">
                                    <input type="checkbox" name="recommend" @if(old('recommend', $Animate->recommend)=='1') checked value="1" @endif data-plugin="switchery" data-color="#7266ba"/>
                                </div>
                            </div>    
                            <div class="form-group">
                                <label class="col-md-2 control-label">首頁釘選圖片</label>
                                <div class="col-md-8">
                                    <input type="file" class="filestyle" data-placeholder="No file" name="pic_recommend">
                                </div>
                                <div class="col-md-2">
                                    <img src="{{$Animate->pic_recommend or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive thumb-md img-rounded">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">首頁圖片(方)</label>
                                <div class="col-md-8">
                                    <input type="file" class="filestyle" data-placeholder="No file" name="pic_index">
                                </div>
                                <div class="col-md-2">
                                    <img src="{{$Animate->pic_index or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive thumb-md img-rounded">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">介紹圖片</label>
                                <div class="col-md-8">
                                    <input type="file" class="filestyle" data-placeholder="No file" name="pic_detail">
                                </div>
                                <div class="col-md-2">
                                    <img src="{{$Animate->pic_detail or url('/ubold/assets/images/blogs/1.jpg') }}" alt="image" class="img-responsive thumb-md img-rounded">
                                </div>
                            </div>
                        </div>
                        
                        <input type="hidden" id="point" value="{{ old('point', $Animate->point) }}">
                        {{-- CSRF 欄位--}}
                        {{ csrf_field() }}
                    </div>
            </div>
                    <div class="text-center p-20">
                        <button type="button" class="btn w-sm btn-white waves-effect" id="cnacelBtn">回列表</button>
                        <button type="submit" class="btn w-sm btn-default waves-effect waves-light" id="sendBtn">送出</button>
                    </div>  
                </form>   
        </div>
    </div>
@endsection
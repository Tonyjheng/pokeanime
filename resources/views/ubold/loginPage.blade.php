<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <meta name="_token" content="{{ csrf_token() }}"/>

        <link rel="shortcut icon" href="assets/images/favicon_1.ico">

        <title>Ubold - 會員登入</title>

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="assets/js/modernizr.min.js"></script>
        
    </head>
    <body>
        
        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
            <div class="panel-heading"> 
                <h3 class="text-center"> 登入到 <strong class="text-custom">UBold</strong> </h3>
            </div> 

            <div class="panel-body">
            <form class="form-horizontal m-t-20" action="login">
                
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" id="email" name="email" placeholder="帳號" 
                        @if(isset($_COOKIE["admin_email"])!=null) value="{{ $_COOKIE["admin_email"] }}" @else value="{{ old('email') }}" @endif >
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" id="password" name="password" placeholder="密碼" 
                        @if(isset($_COOKIE["admin_password"])!=null) value="{{ $_COOKIE["admin_password"] }}" @else value="{{ old('password') }}" @endif>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox" checked>
                            <label for="checkbox-signup">
                                記住我
                            </label>
                        </div>
                        
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="button" id="sendBtn">登入</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        {{-- <a href="page-recoverpw.html" class="text-dark"><i class="fa fa-lock m-r-5"></i> 忘記密碼?</a> --}}
                    </div>
                </div>

            </form> 
            
            </div>   
            </div>                              
                <div class="row">
            	<div class="col-sm-12 text-center">
            		<p>還沒有帳號? <a href="page-register.html" class="text-primary m-l-5"><b>立即註冊</b></a></p>
                        
                    </div>
            </div>
            
        </div>   
        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/detect.js"></script>
        <script src="assets/js/fastclick.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>
        <script src="assets/js/jquery.blockUI.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/js/jquery.scrollTo.min.js"></script>


        <script src="{{url('js/sweetalert2.all.min.js')}}"></script>


        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script>
            $(document).ready(function () {
                $("#sendBtn").on("click", function() {

                    email = $("#email").val();
                    password = $("#password").val();
                    rememberme = $("#checkbox-signup").is(":checked") ? 1 : 0;
                    // swal("帳號:"+email+",密碼:"+password+",記住我:"+rememberme);

                    $.ajax({
                        type: "post",
                        url: "login",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        data: "email="+email+"&password="+password+"&rememberme="+rememberme,
                        success: function(msg){
                            if(msg=='000') {
                                swal({
                                    title: '成功',
                                    text: '登入成功',
                                    // text: msg,
                                    type: 'success',
                                    confirmButtonText: '確定'
                                })
                                setTimeout("location.href=('/ubold/animate')",2000);
                            } else if (msg=='001') {
                                swal({
                                    title: '失敗',
                                    text: '密碼錯誤',
                                    // text: msg,
                                    type: 'error',
                                    confirmButtonText: '確定'
                                })
                            } else if (msg=='002') {
                                swal({
                                    title: '失敗',
                                    text: '禁止登入',
                                    // text: msg,
                                    type: 'error',
                                    confirmButtonText: '確定'
                                })
                            } else if (msg=='003') {
                                swal({
                                    title: '失敗',
                                    text: '查無此帳號',
                                    // text: msg,
                                    type: 'error',
                                    confirmButtonText: '確定'
                                })
                            }else {
                                swal({
                                    title: '失敗',
                                    // text: '失敗',
                                    text: msg,
                                    type: 'warning',
                                    confirmButtonText: '確定'
                                })
                            }
                            // swal(msg);
                        }
                    });
                });
            });
        </script>
	
	</body>
</html>
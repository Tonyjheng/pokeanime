<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
<!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
    <!-- jQuery UI -->
    <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">

    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script>

    <style>
        .watchlistlink {
            font-size: 13px;
            color: #4c4145;
            border: 1px solid #4c4145;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            position: relative;
            padding: 5px 14px 6px 11px;
            -webkit-transition: 0.5s;
            transition: 0.5s;
        }
    </style>
    
@endsection

@section('searchbar')
    @include('layout.searchbar')
@endsection

@section('content')
<div class="col-sm-12">
    <h2 class="page-heading">已播映動畫列表</h2>
    
    {{-- <div class="select-area">
        <form class="select" method='get'>
              <select name="select_item" class="select__sort" tabindex="0">
                <option value="1" selected='selected'>London</option>
                <option value="2">New York</option>
                <option value="3">Paris</option>
                <option value="4">Berlin</option>
                <option value="5">Moscow</option>
                <option value="3">Minsk</option>
                <option value="4">Warsawa</option>
                <option value="5">Kiev</option>
            </select>
        </form>

        <div class="datepicker">
          <span class="datepicker__marker"><i class="fa fa-calendar"></i>Date</span>
          <input type="text" id="datepicker" value='03/10/2014' class="datepicker__input">
        </div>

        <form class="select select--cinema" method='get'>
              <select name="select_item" class="select__sort" tabindex="0">
                <option value="1" selected='selected'>Cineworld</option>
                <option value="2">Empire</option>
                <option value="3">Everyman</option>
                <option value="4">Odeon</option>
                <option value="5">Picturehouse</option>
            </select>
        </form>

        <form class="select select--film-category" method='get'>
              <select name="select_item" class="select__sort" tabindex="0">
                <option value="2" selected='selected'>Children's</option>
                <option value="3">Comedy</option>
                <option value="4">Drama</option>
                <option value="5">Fantasy</option>
                <option value="6">Horror</option>
                <option value="7">Thriller</option>
            </select>
        </form>

    </div> --}}

     <div class="tags-area">
        <div class="tags tags--unmarked">
            <span class="tags__label">篩選:</span>
                <ul>
                    <li class="item-wrap"><a href="/animate/oldlist" class="watchlistlink">全部</a></li>
                    {{-- <li class="item-wrap"><a href="#" class="tags__item" data-filter='社會寫實'>社會寫實</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item" data-filter='科幻未來'>科幻未來</a></li>
                    <li class="item-wrap"><a href="#" class="tags__item" data-filter='奇幻冒險'>奇幻冒險</a></li> --}}
                    @foreach($AnimateType as $AnimateTpKey => $AnimateTpVal)
                    <li class="item-wrap"><a href="/animate/oldlist/{{$AnimateTpKey}}" class="watchlistlink">{{$AnimateTpKey}}</a></li>
                    @endforeach
                </ul>
        </div>
    </div>

    {{-- 快速查看 --}}
    {{-- @foreach($Animates as $Animate)
    <li>{{$Animate->name}}</li>
    @endforeach --}}

    <!-- Movie preview item -->
    @foreach($Animates as $Animate)
    <div class="movie movie--preview movie--full {{$Animate->type}}">
         <div class="col-sm-3 col-md-2 col-lg-2">
                <div class="movie__images">
                    <img alt='' src="{{$Animate->pic_recommend}}">
                </div>
                <div class="movie__feature">
                    {{-- <a href="#" class="movie__feature-item movie__feature--comment">23</a>
                    <a href="#" class="movie__feature-item movie__feature--video">2</a>
                    <a href="#" class="movie__feature-item movie__feature--photo">85</a> --}}
                </div>
        </div>

        <div class="col-sm-9 col-md-10 col-lg-10 movie__about">
                <a href='/animate/{{$Animate->id}}' class="movie__title link--huge">{{$Animate->name}}</a>

                <p class="movie__time">當地首播: {{$Animate->premiere}}</p>

                <p class="movie__option"><strong>原著作者: </strong><a href="javascript:;">{{ $Animate->author }}</a></p>
                <p class="movie__option"><strong>導演監督: </strong><a href="javascript:;">{{ $Animate->director }}</a></p>
                <p class="movie__option"><strong>製作廠商: </strong><a href="javascript:;">{{ $Animate->company }}</a></p>
                <p class="movie__option"><strong>作品平台: </strong><a href="javascript:;">{{ $Animate->mode }}</a></p>
                <p class="movie__option"><strong>播出集數: </strong><a href="javascript:;">{{ $Animate->episodes }}</a></p>
                <p class="movie__option"><strong>作品類型: </strong><a href="javascript:;">{{ $Animate->type }}</a></p>

                <div class="movie__btns">
                    {{-- <a href="#" class="btn btn-md btn--warning">book a ticket <span class="hidden-sm">for this movie</span></a> --}}
                    {{-- <a href="#" class="watchlist">Add to watchlist</a> --}}
                </div>
        </div>

        <div class="clearfix"></div>

    </div>
    @endforeach
    <!-- end movie preview item -->

    @if($Animates->count() == '0')
        <h1>無資料</h1>
        <a href="/animate/oldlist" class="read-more">回列已播映列表</a>
    @endif

    <div class="coloum-wrapper">
        <div class="pagination paginatioon--full">
            @if($Animates->previousPageUrl() != null)
            <a href='{{$Animates->previousPageUrl()}}' class="pagination__prev">prev</a>
            @endif

            @if($Animates->nextPageUrl() != null)
            <a href='{{$Animates->nextPageUrl()}}' class="pagination__next">next</a>
            @endif
        </div>
    </div>

</div>
@endsection


@section('bottomScript')
<!-- JavaScript-->
<!-- jQuery 1.9.1--> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
<!-- Migrate --> 
<script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
<!-- jQuery UI -->
<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<!-- Bootstrap 3--> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<!-- Mobile menu -->
<script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
    <!-- Select -->
<script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script> 

<!-- Stars rate -->
{{-- <script src="{{asset('js/external/jquery.raty.js')}}"></script> --}}

<!-- Form element -->
<script src="{{asset('js/external/form-element.js')}}"></script>
<!-- Form validation -->
<script src="{{asset('js/form.js')}}"></script>

<!-- Custom -->
<script src="{{asset('js/custom.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        init_MovieList();
    });
</script>
@endsection
<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
 <!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->

    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script> 
@endsection

@section('bottomScript')
<!-- JavaScript-->
    <!-- jQuery 1.9.1--> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
    <!-- Migrate --> 
    <script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
    <!-- Bootstrap 3--> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js')}}"></script>

    <!-- Mobile menu -->
    <script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
        <!-- Select -->
    <script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script>

    <!-- Twitter feed -->
    <script src="{{asset('js/external/twitterfeed.js')}}"></script>
    
    <!-- Form element -->
    <script src="{{asset('js/external/form-element.js')}}"></script>
    <!-- Form validation -->
    <script src="{{asset('js/form.js')}}"></script>

    <!-- Custom -->
    <script src="{{asset('js/custom.js')}}"></script>
@endsection

@section('searchbar')
    @include('layout.blogSearchbar')
@endsection

@section('content')
<!-- Main content -->
<section class="container">
    <div class="overflow-wrapper">
        <div class="col-sm-12">
        
            <h2 class="page-heading">{{$title}}</h2>

            @foreach ($Blogs as $blog)
            <!-- News post article-->
            <article class="post post--news">
                <a href='single-page-full.html' class="post__image-link">
                    <img alt='' src="{{$blog->cover}}">
                </a>

                <h1><a href="single-page-full.html" class="post__title-link"></a></h1>
                <p class="post__date">{{$blog->postDate}} </p>

                <div class="wave-devider"></div>

                <p class="post__text">{{$blog->listIntro}}</p> 

                {{-- <div class="tags">
                        <ul>
                            <li class="item-wrap"><a href="#" class="tags__item">news</a></li>
                            <li class="item-wrap"><a href="#" class="tags__item">movie</a></li>
                            <li class="item-wrap"><a href="#" class="tags__item">ceremony</a></li>
                            <li class="item-wrap"><a href="#" class="tags__item">award</a></li>
                        </ul>
                </div> --}}

                <div class="devider-huge"></div>
            </article> 
            <!-- end news post article-->
            @endforeach


            <div class="pagination">
                @if($Blogs->previousPageUrl() != null)
                <a href='{{$Blogs->previousPageUrl()}}' class="pagination__prev">上一頁</a>
                @endif
    
                @if($Blogs->nextPageUrl() != null)
                <a href='{{$Blogs->nextPageUrl()}}' class="pagination__next">下一頁</a>
                @endif
            </div>

        </div>
    </div>
</section>
@endsection
<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
<!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
    <!-- jQuery UI -->
    <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">

    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script>
    
@endsection

@section('bottomScript')
<!-- JavaScript-->
<!-- jQuery 1.9.1--> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
<!-- Migrate --> 
<script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
<!-- Bootstrap 3--> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

<!-- Mobile menu -->
<script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
<!-- Select -->
<script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script> 

<!-- Form element -->
<script src="{{asset('js/external/form-element.js')}}"></script>
<!-- Form validation -->
<script src="{{asset('js/form.js')}}"></script>

<!-- Custom -->
<script src="{{asset('js/custom.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // init_MovieList();
    });
</script>
@endsection

@section('searchbar')
    @include('layout.searchbar')
@endsection

@section('content')
<div class="col-sm-12">
    <h2 class="page-heading">{{$title}}</h2>

    {{-- {{dd($Animates[0]["name"])}} --}}
    {{-- {{dd($Animates)}} --}}

    <div class="cinema-wrap">

        {{-- @php $i=0; @endphp --}}
        {{-- @for($l=0; $l<2; $l++) --}}
        <div class="row">

            @for($d=0; $d<$AnimatesCount; $d++)
            <div class="col-xs-6 col-sm-3 cinema-item">
                <div class="cinema">
                    <a href='/animate/{{$Animates[$d]["id"]}}' class="cinema__images">
                        <img alt='' src="{{$Animates[$d]["pic_index"]}}">
                        <span class="cinema-rating">{{$Animates[$d]["point"]}}</span>
                    </a>
                    <a href="/animate/{{$Animates[$d]["id"]}}" class="cinema-title">{{$Animates[$d]["name"]}}</a>
                </div>
            </div>
            {{-- @php $i= $i+1; @endphp --}}
            @endfor
        </div>
        {{-- @endfor --}}

    </div>

    {{-- <div class="pagination paginatioon--full">
        <a href='#' class="pagination__prev">prev</a>
        <a href='#' class="pagination__next">next</a>
    </div> --}}

    {{-- @for($i=0; $i<$AnimatesCount; $i++)
        <li>{{$Animates[$i]["name"]}}</li>
    @endfor --}}

</div>
@endsection

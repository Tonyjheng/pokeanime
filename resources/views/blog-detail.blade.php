<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
<!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->
    <!-- <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> -->
    
    <!-- Swiper slider -->
    <link href="{{asset('css/external/idangerous.swiper.css')}}" rel="stylesheet" />
    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script> 
@endsection

@section('bottomScript')
<!-- JavaScript-->
    <!-- jQuery 1.9.1--> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
    <!-- Migrate --> 
    <script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
    <!-- Bootstrap 3--> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <!-- Swiper slider -->
    <script src="{{asset('js/external/idangerous.swiper.min.js')}}"></script>

    <!-- Mobile menu -->
    <script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
    <!-- Select -->
    <script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script>
    <!-- Share buttons -->
    <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-525fd5e9061e7ef0"></script>

    <!-- Twitter feed -->
    {{-- <script src="{{asset('js/external/twitterfeed.js')}}"></script> --}}
    
    <!-- Form element -->
    <script src="{{asset('js/external/form-element.js')}}"></script>
    <!-- Form validation -->
    <script src="{{asset('js/form.js')}}"></script>

    <!-- Custom -->
    <script src="{{asset('js/custom.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // init_SinglePage();
        });
    </script>
    
    
@endsection

@section('searchbar')
    @include('layout.blogSearchbar')
@endsection

@section('content')
<!-- Main content -->
<section class="container">
    <div class="col-sm-12">
        <h2 class="page-heading">{{ $title }}</h2>

        <div class="post">

            <img src="{{ $Blog->cover }}" alt="{{ $Blog->title }}" width="100%">

            <h1>{{ $Blog->title }}</h1>
            <p class="post__date">{{ $Blog->postDate }} </p>

            <div class="wave-devider"></div>

            {!! $Blog->content !!}

            {{-- <div class="info-wrapper">
                <div class="tags">
                    <ul>
                        <li class="item-wrap"><a href="#" class="tags__item">news</a></li>
                        <li class="item-wrap"><a href="#" class="tags__item">movie</a></li>
                        <li class="item-wrap"><a href="#" class="tags__item">ceremony</a></li>
                        <li class="item-wrap"><a href="#" class="tags__item">award</a></li>
                    </ul>
                </div>
            </div> --}}
        </div>

        {{-- <h2 class="page-heading">Similar posts</h2>

        <div class="col-sm-4 col--remove">
            <div class="post post--preview post--preview--full">
                <div class="post__image">
                    <img alt='' src="http://placehold.it/270x330">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">22 October 2013 </p>
                <a href="#" class="post__title">"Thor: The Dark World" - World Premiere</a>
                <a href="#" class="btn read-more post--btn">read more</a>
            </div>
        </div>
        <div class="col-sm-4 col--remove">
            <div class="post--preview post--preview--full">
                <div class="post__image">
                    <img alt='' src="http://placehold.it/270x330">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">22 October 2013 </p>
                <a href="#" class="post__title">30th Annual Night Of Stars Presented By The Fashion ...</a>
                <a href="#" class="btn read-more post--btn">read more</a>
            </div>
        </div>
        <div class="col-sm-4 col--remove">
            <div class="post--preview post--preview--full">
                <div class="post__image">
                    <img alt='' src="http://placehold.it/270x330">
                    <div class="social social--position social--hide">
                        <span class="social__name">Share:</span>
                        <a href='#' class="social__variant social--first fa fa-facebook"></a>
                        <a href='#' class="social__variant social--second fa fa-twitter"></a>
                        <a href='#' class="social__variant social--third fa fa-vk"></a>
                    </div>
                </div>
                <p class="post__date">22 October 2013 </p>
                <a href="#" class="post__title">Hollywood Film Awards 2013</a>
                <a href="#" class="btn read-more post--btn">read more</a>
            </div>
        </div> --}}

        {{-- <div class="clearfix"></div>
        <h2 class="page-heading">comments (15)</h2>

        <div class="comment-wrapper">
            <form id="comment-form" class="comment-form" method='post'>
                <textarea class="comment-form__text" placeholder='Add you comment here'></textarea>
                <label class="comment-form__info">250 characters left</label>
                <button type='submit' class="btn btn-md btn--danger comment-form__btn">add comment</button>
            </form>

            <div class="comment-sets">

                <div class="comment">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Roberta Inetti</a>
                    <p class="comment__date">today | 03:04</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Olia Gozha</a>
                    <p class="comment__date">22.10.2013 | 14:40</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment comment--answer">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Dmitriy Pustovalov</a>
                    <p class="comment__date">today | 10:19</p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div class="comment comment--last">
                    <div class="comment__images">
                        <img alt='' src="http://placehold.it/50x50">
                    </div>

                    <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Sia Andrews</a>
                    <p class="comment__date"> 22.10.2013 | 12:31 </p>
                    <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                    <a href='#' class="comment__reply">Reply</a>
                </div>

                <div id='hide-comments' class="hide-comments">
                    <div class="comment">
                        <div class="comment__images">
                            <img alt='' src="http://placehold.it/50x50">
                        </div>

                        <a href='#' class="comment__author"><span class="social-used fa fa-facebook"></span>Roberta Inetti</a>
                        <p class="comment__date">today | 03:04</p>
                        <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                        <a href='#' class="comment__reply">Reply</a>
                    </div>

                    <div class="comment">
                        <div class="comment__images">
                            <img alt='' src="http://placehold.it/50x50">
                        </div>

                        <a href='#' class="comment__author"><span class="social-used fa fa-vk"></span>Olia Gozha</a>
                        <p class="comment__date">22.10.2013 | 14:40</p>
                        <p class="comment__message">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae enim sollicitudin, euismod erat id, fringilla lacus. Cras ut rutrum lectus. Etiam ante justo, volutpat at viverra a, mattis in velit. Morbi molestie rhoncus enim, vitae sagittis dolor tristique et.</p>
                        <a href='#' class="comment__reply">Reply</a>
                    </div>
                </div>

                <div class="comment-more">
                    <a href="#" class="watchlist">Show more comments</a>
                </div> --}}

            </div>
        </div>
    </div>
</section>
@endsection
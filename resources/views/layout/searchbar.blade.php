<!-- Search bar -->
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method="POST" action="/animate/search" class="search">
            <input type="text" class="search__field" placeholder="搜尋..." name="keyword">
            <select name="search_item" id="search-sort" class="search__sort" tabindex="0">
                <option value="name" selected='selected'>名稱</option>
                <option value="author">作者</option>
                <option value="director">導演監督</option>
                <option value="company">製作廠商</option>
                <option value="cast">聲優</option>
                <option value="proxy">台灣代理</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">--- 搜尋所有動畫 ----</button>
            {{-- CSRF 欄位--}}
            {{ csrf_field() }}
        </form>
    </div>
</div>
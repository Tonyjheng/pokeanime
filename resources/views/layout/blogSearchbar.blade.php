<!-- Search bar -->
<div class="search-wrapper">
    <div class="container container--add">
        <form id='search-form' method='get' class="search" action="/blog/search">
            <input type="text" class="search__field" placeholder="搜尋" name="keyword">
            <select name="type" id="search-sort" class="search__sort" tabindex="0">
                <option value="title" selected='selected'>標題</option>
                <option value="postdate">發佈年份</option>
            </select>
            <button type='submit' class="btn btn-md btn--danger search__button">-------搜尋文章--------</button>
        </form>
    </div>
</div>
<!-- 指定繼承 layout.master 母模板 -->
@extends('layout.master')

@section('head')
<!-- Fonts -->
    <!-- Font awesome - icon font -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- Roboto -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>

<!-- Stylesheets -->

    <!-- Mobile menu -->
    <link href="{{asset('css/gozha-nav.css')}}" rel="stylesheet" />
    <!-- Select -->
    <link href="{{asset('css/external/jquery.selectbox.css')}}" rel="stylesheet" />

    <!-- Custom -->
    <link href="{{asset('css/style.css?v=1')}}" rel="stylesheet" />

    <!-- Modernizr --> 
    <script src="{{asset('js/external/modernizr.custom.js')}}"></script>

    <style>
        .align-center {
            text-align: center;
        }
        .star-size {
            font-size: 1.5em;
            color: #ba5091;
        }
    </style>

@endsection

@section('bottomScript')
<!-- JavaScript-->
    <!-- jQuery 1.9.1--> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
    <!-- Migrate --> 
    <script src="{{asset('js/external/jquery-migrate-1.2.1.min.js')}}"></script>
    <!-- Bootstrap 3--> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

    <!-- Mobile menu -->
    <script src="{{asset('js/jquery.mobile.menu.js')}}"></script>
        <!-- Select -->
    <script src="{{asset('js/external/jquery.selectbox-0.2.min.js')}}"></script>
    <!-- Stars rate -->
    <script src="{{asset('js/external/jquery.raty.js')}}"></script>

    <!-- Form element -->
    <script src="{{asset('js/external/form-element.js')}}"></script>
    <!-- Form validation -->
    <script src="{{asset('js/form.js')}}"></script>

    <!-- Custom -->
    <script src="{{asset('js/custom.js')}}"></script> 
    
    <script type="text/javascript">
        $(document).ready(function() {
            init_Rates();
        });
    </script>
@endsection

@section('searchbar')
    @include('layout.searchbar')
@endsection

@section('content')
<!-- Main content -->
<section class="container">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-8 col-md-9">
                <h2 class="page-heading">{{$title}}</h2>

                <div class="rates-wrapper">
                    
                    <table>
                        <colgroup class="col-width-lg">
                        <colgroup class="col-width">
                        <colgroup class="col-width-sm">
                        <colgroup class="col-width">

                        {{-- <tr class="rates rates--top">
                            <td class="rates__obj"><a href="#" class="rates__obj-name">1. Thor: The Dark World</a></td>
                            <td class="rates__vote">233 546 votes</td>
                            <td class="rates__result">5.0</td>
                            <td class="rates__stars align-center">
                                <i class="fa fa-star star-size"></i>
                                <i class="fa fa-star star-size"></i>
                                <i class="fa fa-star star-size"></i>
                                <i class="fa fa-star star-size"></i>
                                <i class="fa fa-star star-size"></i>
                            </td>
                        </tr> --}}

                        @php $i = 1; @endphp
                        @foreach($Animates as $Animate)
                        <tr class="rates @if( $i == '1' or $i =='2' or $i=='3' ) rates--top @endif">
                            <td class="rates__obj"><a href="/animate/{{$Animate->id}}" target="_blank" class="rates__obj-name">{{$Animate->name}}</a></td>
                            <td class="rates__vote">{{$Animate->author}} / {{$Animate->company}}</td>
                            <td class="rates__result">{{$Animate->point}}</td>
                            <td class="rates__stars align-center">
                                @for( $s=0; $s < $Animate->point; $s++ )
                                <i class="fa fa-star star-size"></i>
                                @endfor
                            </td>
                        </tr>
                        @php $i++; @endphp
                        @endforeach

                    </table>
                </div>

                <div class="pagination paginatioon--full coloum-wrapper">
                    @if($Animates->previousPageUrl() != null)
                    <a href='{{$Animates->previousPageUrl()}}' class="pagination__prev">prev</a>
                    @endif
        
                    @if($Animates->nextPageUrl() != null)
                    <a href='{{$Animates->nextPageUrl()}}' class="pagination__next">next</a>
                    @endif
                </div>    
            </div>

            <aside class="col-sm-4 col-md-3">
                <div class="sitebar hidden-xs">
                    <div class="banner-wrap">
                        <img alt='banner' src="http://placehold.it/500x500">
                    </div>

                        <div class="banner-wrap">
                        <img alt='banner' src="http://placehold.it/500x500">
                    </div>

                        <div class="banner-wrap banner-wrap--last">
                        <img alt='banner' src="http://placehold.it/500x500">
                    </div>

                </div>
            </aside>
        </div>
    </div>
</section>
@endsection



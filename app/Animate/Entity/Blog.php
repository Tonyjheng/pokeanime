<?php

namespace App\Animate\Entity;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    // 資料表名稱
    protected $table = 'blog';

    //主鍵名稱
    protected $primaryKey = 'id';

    //大量異動欄位
    protected $fillable = [
        "id",
        "title",
        "postDate",
        "listIntro",
        "content",
        "cover",
        "cover_index",
        "remark",
        "hits",
        "sort",
        "seal",
        "show",
    ];
}

<?php

namespace App\Animate\Entity;

use Illuminate\Database\Eloquent\Model;

class Animate extends Model
{
    // 資料表名稱
    protected $table = 'animate';

    //主鍵名稱
    protected $primaryKey = 'id';

    //大量異動欄位
    protected $fillable = [
        "id",
        "status",
        "name",
        "name_jp",
        "read",
        "introduction",
        "myreview",
        "point",
        "type",
        "mode",
        "premiere",
        "episodes",
        "author",
        "director",
        "company",
        "proxy",
        "website",
        "pv",
        "filesize",
        "subtitle",
        "storage",
        "level",
        "cast",
        "staff",
        "gamer",
        "bilibili",
        "iqiyi",
        "recommend",
        "pic_recommend",
        "pic_index",
        "pic_detail",
        "remark",
        "hits",
        "sort",
    ];

    public function subtitles()
    {
        return $this->hasone('App\Animate\Entity\Animate_subtitles' , 'id' , 'subtitle');
    }
}

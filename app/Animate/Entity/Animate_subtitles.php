<?php

namespace App\Animate\Entity;

use Illuminate\Database\Eloquent\Model;

class Animate_subtitles extends Model
{
    // 資料表名稱
    protected $table = 'animate_subtitles';

    //主鍵名稱
    protected $primaryKey = 'id';

    //大量異動欄位
    protected $fillable = [
        "id",
        "subtitles_name",
        "sort",
        "remark",
    ];
}

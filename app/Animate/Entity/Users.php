<?php

namespace App\Animate\Entity;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    // 資料表名稱
    protected $table = 'users';

    //主鍵名稱
    protected $primaryKey = 'id';

    //大量異動欄位
    protected $fillable = [
        "id",
        "name",
        "nickname",
        "email",
        "type",
        "seal",
        "status",
        "password",
        "remember_token",
        "remark",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

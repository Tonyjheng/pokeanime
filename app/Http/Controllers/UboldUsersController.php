<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Users;
use Validator;  // 驗證器
use Hash;       // 雜湊

class UboldUsersController extends Controller
{
    //會員列表
    public function userListPage()
    { 

        $Users = Users::all()->where('seal', '!=', 1)->sortBy('create_at');
        // $Users = Users::all()->sortBy('create_at');
        $UsersSeals = Users::all()->where('seal', '=', 1)->sortBy('create_at');
   
        $binding = [
            'title' => '後台會員列表',
            'Users' => $Users,
            'UsersSeals' => $UsersSeals,
        ];
 
        return view('ubold.userListPage' , $binding);
        // return dd($Users->toArray());
    }

    //新增會員頁面
    public function userAddPage()
    {
        $binding = [
            'title' => '新增會員',
        ];
        return view('ubold.userAddPage', $binding);
    }
    
    //新增會員資料處理
    public function userAddProcess()
    {
        // 接收輸入資料
        $input = request()->all();

        $input["seal"] = (isset($input["seal"]))?"1":"0";
        $input["status"] = (isset($input["status"]))?"1":"0";
        $input["remember_token"] = csrf_token();

        // return dd($input);
        
        // 驗證規則
        $rules = [
            // 姓名
            'name'=> [
                'required',
            ],
            // 暱稱
            'nickname'=> [
                'required',
                'max:50',
            ],
            // Email
            'email'=> [
                'required',
                'max:150',
                'email',
            ],
            // 密碼
            'password' => [
                'required',
                'same:password_confirmation',
                'min:6',
            ],
            // 密碼驗證
            'password_confirmation' => [
                'required',
                'min:6',
            ],
            // 帳號類型
            'type' => [
                'required',
                'in:G,A'
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/users/userAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // 密碼加密
        $input['password'] = Hash::make($input['password']);

        // return dd($input);

        // 新增資料
        $Users = Users::create($input);

        // 重新導向到登入頁
        return redirect('/ubold/users');
    }

    //更新會員資料頁面
    public function userEditPage($user_id)
    {
        
        $Users = Users::findOrFail($user_id);
    
        $binding = [
            'title' => '後台會員資料更新',
            'User' => $Users,
        ];
    
        return view('ubold.userEditPage' , $binding);
        // return dd($Users->toArray());
    }

    // 會員資料更新處理
    public function userUpdateProcess($user_id)
    {
        // 撈取會員資料
        $User = Users::findOrFail($user_id);
        // 接收輸入資料
        $input = request()->all();

        $input["seal"] = (isset($input["seal"]))?"1":"0";
        $input["status"] = (isset($input["status"]))?"1":"0";
    
        // 驗證規則
        $rules = [
            // 姓名
            'name'=> [
                'required',
            ],
            // 暱稱
            'nickname'=> [
                'required',
                'max:50',
            ],
            // Email
            'email'=> [
                'required',
                'max:150',
                'email',
            ],
            // 帳號類型
            'type' => [
                'required',
                'in:G,A'
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/users/' . $User->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }

        if($input["password"]!=null) {

            $checkPW["password"] = $input["password"];
            $checkPW["password_confirmation"] = $input["password_confirmation"];

            $rulesPW = [
                // 密碼
                'password' => [
                    'required',
                    'same:password_confirmation',
                    'min:6',
                ],
                // 密碼驗證
                'password_confirmation' => [
                    'required',
                    'min:6',
                ],
            ];

            // 驗證資料
            $validatorPW = Validator::make($checkPW, $rulesPW);
        
            if ($validatorPW->fails()) {
                // 資料驗證錯誤
                return redirect('/ubold/users/' . $User->id . '/edit')
                    ->withErrors($validatorPW)
                    ->withInput();
            }

            // 密碼加密
            $input['password'] = Hash::make($input['password']);
        } else {
            $input["password"] = $User->password;
        }
        
        // return dd($input);

        // 商品資料更新
        $User->update($input);
        
        // 重新導向到商品編輯頁
        return redirect('/ubold/users/' . $User->id . '/edit');
    }

    // 會員資料處理-刪除
    public function userDeleteProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $UserCheck = Users::where('id', $input['id'])->count();
        if($UserCheck=='0') {
            return '001';
        }

        // 撈取會員資料
        $User = Users::findOrFail($input['id']);
        // return dd($User->toArray());

        // 刪除會員
        if( $User->delete($input['id']) ) {
            return '000';
        } else {
            return '001';
        };
        return '001';

    }

    // 會員資料處理-封存
    public function userSealProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $UserCheck = Users::where('id', $input['id'])->count();
        if($UserCheck=='0') {
            return '001';
        }

        // 撈取會員資料
        $User = Users::findOrFail($input['id']);
        // return dd($User->toArray());

        $User->seal = '1';

        // 封存文章
        if( $User->save() ) {
            return '000';
        } else {
            return '001';
        };
        
        return '001';

    }

    public function userSealCancelProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $UserCheck = Users::where('id', $input['id'])->count();
        if($UserCheck=='0') {
            return '001';
        }

        // 撈取會員資料
        $User = Users::findOrFail($input['id']);
        
        $User->seal = '0';
        // return "取消封存使用者:".$User['name'];

        // 封存文章
        if( $User->save() ) {
            return '000';
        } else {
            return '001';
        };
        
        return '001';

    }

    //會員登入畫面
    public function loginPage() 
    {
        $binding = [
            'title' => '會員登入',
        ];
        return view('ubold.loginPage', $binding);
    }

    //登入資料驗證
    public function loginProcess() 
    {
        // 接收輸入資料
        $input = request()->all();
        // return dd($input);

        //確認是否有該筆資料
        $UserCheck = Users::where('email', $input['email'])->count();
        if($UserCheck=='0') {
            return '003';
        }
        
        // 撈取使用者資料
        $User = Users::where('email', $input['email'])->firstOrFail();
        // return dd($User->toArray());
        
        // 檢查密碼是否正確
        $is_password_correct = Hash::check($input['password'], $User->password);
        
        if (!$is_password_correct) {
            // 密碼錯誤回傳錯誤訊息
            return '001';
        }

        if ($User->status == '1') {
            // 會員禁止登入
            return '002';
        }
        
        // session 紀錄會員編號
        session()->put('user_id', $User->id);
        
        // 重新導向到原先使用者造訪頁面，沒有嘗試造訪頁則重新導向回首頁
        // return redirect()->intended('ubold/animate');

        //記住帳號密碼
        if($input['rememberme']=="1"){
			setcookie("admin_email", $input['email'] , time()+365*24*60);
            setcookie("admin_password", $input['password'] , time()+365*24*60);
		}else{
			if(isset($_COOKIE["admin_email"])){
				setcookie("admin_email", $input['email'] , time()-100);
				setcookie("admin_password", $input['password'] , time()-100);
			}
        }
        
        return '000';
    }

    // 處理登出資料
    public function logoutProcess(){
        // 清除 Session
        session()->forget('user_id');
        
        // 重新導向回首頁
        return redirect('/');
    }

}

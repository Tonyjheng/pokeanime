<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Blog;
use DB;

class BlogController extends Controller
{ 
    // 文章列表
    public function blogListPage()
    { 
        DB::connection()->enableQueryLog();

        // 撈取文章資料
        $Blogs = Blog::where('show', '=', 1)
                            ->where('seal', '=', 0)
                            ->orderBy('postDate','DESC')
                            ->paginate(2);

        $BlogsPop = Blog::where('show', '=', 1)
                        ->where('seal', '=', 0)
                        ->orderBy('hits','DESC')
                        ->take(7)
                        ->get();

        // return dd(DB::getQueryLog());
        // return dd($Blogs->toArray());
        // return dd($BlogsPop->toArray());


        foreach ($Blogs as $Blog) {
            if (!is_null($Blog->cover)) {
                // 設定網址
                $Blog->cover = url($Blog->cover);
            }
        }

        // return dd($Blogs->toArray());
        
        $binding = [
            'title' => '文章',
            'Blogs'=> $Blogs,
            'BlogsPop'=> $BlogsPop,
        ];
        return view('blog-list', $binding);
    }

    public function blogSearchPage()
    { 
        DB::connection()->enableQueryLog();

        $input = request()->all();
        // dd($input);

        if(isset($input['keyword'])==null) {

            $input['keyword'] = "新番";
            $input['type'] = "title";

            $Blogs = Blog::where('show', '=', 1)
                                ->where('title', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('hits' , 'desc')
                                ->paginate(2);
        }

        if($input['type']=="title") {

            $Blogs = Blog::where('show', '=', 1)
                                ->where('title', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('postDate' , 'desc')
                                ->paginate(2);

        } else if($input['type']=="postdate") {

            $Blogs = Blog::where('show', '=', 1)
                                ->where('postDate', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('postDate' , 'desc')
                                ->paginate(2);

        } else {

            $input['keyword'] = "熱門";
            $input['type'] = "title";

            $Blogs = Blog::where('show', '=', 1)
                                ->where('title', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('hits' , 'desc')
                                ->paginate(2);
        } 
        
        $Blogs->appends([ 'type' => $input['type'] , 'keyword' => $input['keyword'] ])->links();

        foreach ($Blogs as $Blog) {
            if (!is_null($Blog->cover)) {
                // 設定網址
                $Blog->cover = url($Blog->cover);
            }
        }

        foreach ($Blogs as $Blog) {
            if (!is_null($Blog->cover_index)) {
                // 設定網址
                $Blog->cover_index = url($Blog->cover_index);
            }
        }

        // return dd(DB::getQueryLog());
        // return dd($Blogs->toArray());
        // return dd($Blogs);
        
        $binding = [
            'title' => '"'.$input['keyword'].'"文章搜尋結果',
            'Blogs' => $Blogs,
        ];
        return view('blog-search', $binding);

    }

    // 文章詳細頁面
    public function blogDetailPage($blod_id)
    { 
        // 撈取文章資料
        $Blog = Blog::findOrFail($blod_id);

        if (!is_null($Blog->cover)) {
            // 設定圖片網址
            $Blog->cover = url($Blog->cover);
        }

        // return dd($Blog->toArray());

        $binding = [
            'title' => $Blog->title,
            'Blog'=> $Blog,
        ];
        return view('blog-detail', $binding);
        // return $Animate_subtitles;
    }

}
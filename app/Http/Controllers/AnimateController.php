<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Animate;
use App\Animate\Entity\Blog;
use DB;

class AnimateController extends Controller
{
    //首頁動畫
    public function index()
    { 
        $Animate_recommend = Animate::all()
                        ->where('show', '=', 1)
                        ->where('recommend', '=', 1)
                        ->sortBy('premiere');
        $Animate_recommend_Count = Animate::all()
                        ->where('show', '=', 1)
                        ->where('recommend', '=', 1)
                        ->count();

        $Animate_shinban = Animate::all()
                            ->sortBy('sort')
                            ->where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->take(8);
        $Animate_shinban_Count = Animate::all()
                            ->where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->count();

        $Blogs = Blog::all()
                ->sortByDesc('created_at')
                ->where('show', '=', 1)
                ->where('seal', '!=', 1)
                ->take(3);

        //上方推薦動畫
        // 設定圖片網址
        foreach ($Animate_recommend as $Animate_re) {
            if (!is_null($Animate_re->pic_recommend)) {
                // 設定網址
                $Animate_re->pic_recommend = url($Animate_re->pic_recommend);
            }
        }

        if($Animate_recommend_Count>0) {
            $i = 0;
            foreach ($Animate_recommend as $Animate_re_array) {
                $Animate_recommend_toArray[$i]["pic_recommend"] = $Animate_re_array["pic_recommend"];
                $Animate_recommend_toArray[$i]["id"] = $Animate_re_array["id"];
                $Animate_recommend_toArray[$i]["name"] = $Animate_re_array["name"];
                $Animate_recommend_toArray[$i]["point"] = $Animate_re_array["point"];
                $Animate_recommend_toArray[$i]["premiere"] = $Animate_re_array["premiere"];
                $Animate_recommend_toArray[$i]["author"] = $Animate_re_array["author"];
                $Animate_recommend_toArray[$i]["director"] = $Animate_re_array["director"];
                $Animate_recommend_toArray[$i]["company"] = $Animate_re_array["company"];
                $i++;
            }
        } else {
            $Animate_recommend_toArray = null;
        }

        //下方動畫區(格子)
        // 設定圖片網址
        foreach ($Animate_shinban as $Animate_shin) {
            if (!is_null($Animate_shin->pic_index)) {
                // 設定網址
                $Animate_shin->pic_index = url($Animate_shin->pic_index);
            }
        }

        //最新文章- 設定圖片網址
        foreach ($Blogs as $Blog) {
            if (!is_null($Blog->cover_index)) {
                // 設定網址
                $Blog->cover_index = url($Blog->cover_index);
            }
        }

        $binding = [
            'title' => '首頁',
            'Animate_recommend_toArray' => $Animate_recommend_toArray,
            'Animate_recommend_Count' => $Animate_recommend_Count,
            'Animate_shinban' => $Animate_shinban,
            'Blogs' => $Blogs,
        ];
 
        // return dd($Animate_recommend);
        // return dd($Animate_recommend->toArray());
        // return dd($Animate_recommend_toArray);
        // return dd($Animate_shinban->toArray());
        // return dd($Blogs->toArray());
        return view('index' , $binding);
    }

    // 動畫詳細頁面
    public function animateDetailPage($animate_id)
    { 
        // 撈取動畫資料
        $Animate = Animate::findOrFail($animate_id);
        // return dd($Animate->toArray());

        if (!is_null($Animate->pic_detail)) {
            // 設定圖片網址
            $Animate->pic_detail = url($Animate->pic_detail);
        }

        $binding = [
            'title' => $Animate->name,
            'Animate'=> $Animate,
        ];
        return view('animate-detail', $binding);
        // return $Animate_subtitles;
    }
    
    // 新番動畫列表
    public function animateListPage()
    { 
        // 撈取新番動畫資料

        //無分頁寫法
        $Animates = Animate::all()
                            ->where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->sortBy('premiere');
        
        //上方篩選類別
        $AnimateType = Animate::all()->where('shinban', '=', 1)
                                     ->groupBy('type');
        
        //完整分頁寫法
        /* $Animates = Animate::where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->orderBy('premiere');
                            ->paginate(50); */

        //簡易分頁的寫法->暫不套用
        /* $Animates2 = Animate::orderBy('premiere')
                            ->where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->simplePaginate(3); */

        // return dd($Animates->toArray());
        // return dd($Animates2->toArray());
        // return dd($AnimateType->toArray());

        foreach ($Animates as $Animate) {
            if (!is_null($Animate->pic_recommend)) {
                // 設定網址
                $Animate->pic_recommend = url($Animate->pic_recommend);
            }
        }

        // return dd($Animates->toArray());
        
        $binding = [
            'title' => '新番動畫列表',
            'Animates'=> $Animates,
            'AnimateType'=> $AnimateType,
        ];
        return view('animate-list', $binding);
        // return $Animate_subtitles;
    }


    // 舊番動畫列表
    public function animateoldListPage()
    { 
        // 撈取舊番動畫資料
        $Animates = Animate::where('show', '=', 1)
                            ->where('shinban', '!=', 1)
                            ->orderBy('premiere')
                            ->paginate(3);
        
        //上方篩選類別
        $AnimateType = Animate::all()
                                ->where('shinban', '!=', 1)
                                ->groupBy('type');

        // return dd($Animates->toArray());
        // return dd($AnimateType->toArray());

        foreach ($Animates as $Animate) {
            if (!is_null($Animate->pic_recommend)) {
                // 設定網址
                $Animate->pic_recommend = url($Animate->pic_recommend);
            }
        }

        // return dd($Animates->toArray());
        
        $binding = [
            'title' => '舊番動畫列表',
            'Animates'=> $Animates,
            'AnimateType'=> $AnimateType,
        ];
        return view('animateold-list', $binding);
        // return $Animate_subtitles;
    }

    // 舊番動畫列表-分類篩選
    public function animateoldTypeListPage($type)
    { 

        // 撈取舊番動畫資料
        $Animates = Animate::where('show', '=', 1)
                            ->where('type', '=', $type)
                            ->where('shinban', '!=', 1)
                            ->orderBy('premiere')
                            ->paginate(25);
        
        //上方篩選類別
        $AnimateType = Animate::all()
                                ->where('shinban', '!=', 1)
                                ->groupBy('type');

        // return dd($Animates->toArray());  
        // return dd($AnimateType->toArray());
        
        // 測試 -> 把陣列轉為Json或陣列,就可以拿到下一頁的連結
        /* $AnimatesJson = $Animates->toJson(JSON_UNESCAPED_UNICODE);
        $AnimatesJson2 = json_decode($AnimatesJson, true);
        return dd($AnimatesJson2['last_page_url']); */

        foreach ($Animates as $Animate) {
            if (!is_null($Animate->pic_recommend)) {
                // 設定網址
                $Animate->pic_recommend = url($Animate->pic_recommend);
            }
        }

        // return dd($Animates->toArray());
        
        $binding = [
            'title' => '舊番動畫列表',
            'Animates'=> $Animates,
            'AnimateType'=> $AnimateType,
        ];
        return view('animateold-list', $binding);
        // return $Animate_subtitles;
    }

    // 新番排行
    public function animateRateListPage()
    { 
        // 撈取新番動畫資料
        // 完整分頁寫法
        $Animates = Animate::where('show', '=', 1)
                            ->where('shinban', '=', 1)
                            ->orderBy('point' , 'desc')
                            ->orderBy('sort' , 'asc')
                            ->paginate(30);

        // return dd($Animates->toArray());
        
        $binding = [
            'title' => '新番動畫排行',
            'Animates'=> $Animates,
        ];
        return view('animate-rate', $binding);
        // return $Animate_subtitles;
    }


     // 舊番動畫排行
     public function animateoldRateListPage()
     { 
         // 撈取舊番動畫資料
         $Animates = Animate::where('show', '=', 1)
                             ->where('shinban', '!=', 1)
                             ->orderBy('point' , 'desc')
                             ->orderBy('sort' , 'asc')
                             ->paginate(2);
         
        //  return dd($Animates->toArray());
         
         $binding = [
             'title' => '已播映動畫排行',
             'Animates'=> $Animates,
         ];
         return view('animate-oldrate', $binding);
         // return $Animate_subtitles;
     }

    // 搜尋頁面
    public function animateSearchPage()
    { 
        DB::connection()->enableQueryLog();

        $input = request()->all();
        // dd($input);

        // 完整分頁寫法
        if($input['search_item']=="cast") {
            $Animates = Animate::where('show', '=', 1)
                                ->where('cast', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();
            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('cast', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } else if($input['search_item']=="author") {
            $Animates = Animate::where('show', '=', 1)
                                ->where('author', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();
            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('author', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } else if($input['search_item']=="director") {
            $Animates = Animate::where('show', '=', 1)
                                ->where('director', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();
            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('director', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } else if($input['search_item']=="company") {
            $Animates = Animate::where('show', '=', 1)
                                ->where('company', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();
            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('company', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } else if($input['search_item']=="proxy") {
            /* $Animates = Animate::where('show', '=', 1)
                                ->where('proxy', 'like', '%'.$input['keyword'].'%')
                                ->paginate(10); */
            $Animates = Animate::where('show', '=', 1)
                                ->where('proxy', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();
            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('proxy', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } else {
            $Animates = Animate::where('show', '=', 1)
                                ->where('name', 'like', '%'.$input['keyword'].'%')
                                ->orderBy('premiere' , 'desc')
                                ->get();

            $AnimatesCount = Animate::where('show', '=', 1)
                                    ->where('name', 'like', '%'.$input['keyword'].'%')
                                    ->count();
        } 


        if($input['keyword']==null) {
             $Animates = Animate::where('show', '=', 1)
                                 ->orderBy('premiere' , 'desc')
                                 ->take(8)
                                 ->get();
             $AnimatesCount = '8';
         }

        foreach ($Animates as $Animate) {
            if (!is_null($Animate->pic_index)) {
                // 設定網址
                $Animate->pic_index = url($Animate->pic_index);
            }
        }

        // return dd(DB::getQueryLog());
        // return dd($Animates->toArray());
        // return $AnimatesCount;
        // return dd($Animates);
        
        $binding = [
            'title' => '"'.$input['keyword'].'"搜尋結果',
            'Animates'=> $Animates->toArray(),
            'AnimatesCount'=> $AnimatesCount,
        ];
        return view('animate-search', $binding);

    }

}
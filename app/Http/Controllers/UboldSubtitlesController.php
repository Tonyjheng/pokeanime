<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Animate_subtitles;
use Validator;  // 驗證器

class UboldSubtitlesController extends Controller
{

    //新增字幕組頁面
    public function subtitlesAddPage()
    {
        $binding = [
            'title' => '新增字幕組',
        ];
        return view('ubold.subtitlesAddPage', $binding);
    }

    //處理字幕組資料
    public function subtitlesAddProcess()
    {
        // 接收輸入資料
        $input = request()->all();

        // return "名稱=".$input['name'].",排序=".$input['sort'].",備註=".$input['remark'];
        
        // 驗證規則
        $rules = [
            // 名稱
            'subtitles_name'=> [
                'required',
            ],
            // 排序
            'sort'=> [
                'required',
                'integer',
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/subtitlesAdd')
                ->withErrors($validator)
                ->withInput();
        }

        // return dd($input);

        // 新增字幕組資料
        $Users = Animate_subtitles::create($input);

        // 重新導向到登入頁
        return redirect('/ubold/subtitles');
    }

    //更新字幕組資料頁面
    public function subtitlesEditPage($subtitles_id)
    {
        
        $Subtitles = Animate_subtitles::findOrFail($subtitles_id);
    
        $binding = [
            'title' => '後台字幕組資料更新',
            'Subtitles' => $Subtitles,
        ];
    
        return view('ubold.subtitlesEditPage' , $binding);
        // return "後台字幕組列表";
        // return dd($Subtitles);
    }

    // 字幕組資料更新處理
    public function subtitlesUpdateProcess($subtitles_id)
    {
        // 撈取字幕組資料
        $Subtitles = Animate_subtitles::findOrFail($subtitles_id);
        // 接收輸入資料
        $input = request()->all();

        // return dd($input);
    
        // 驗證規則
        $rules = [
            // 名稱
            'subtitles_name'=> [
                'required',
            ],
            // 排序
            'sort'=> [
                'required',
                'integer',
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/' . $Subtitles->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
    
        // 商品資料更新
        $Subtitles->update($input);
        
        // 重新導向到商品編輯頁
        return redirect('/ubold/subtitles');
    }

    // 字幕組資料處理-刪除
    public function subtitlesDeleteProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $SubtitlesCheck = Animate_subtitles::where('id', $input['id'])->count();
        if($SubtitlesCheck=='0') {
            return '001';
        }

        // 撈取字幕組資料
        $Subtitles = Animate_subtitles::findOrFail($input['id']);
        // return dd($Subtitles->toArray());

        // 刪除字幕組
        if( $Subtitles->delete($input['id']) ) {
            return '000';
        } else {
            return '001';
        };
        return '001';

        // return $Subtitles['subtitles_name'];
    }

    //字幕組列表
    public function subtitlesListPage()
    {
        //每頁資料量
        $row_per_page = 1;
        //撈取字幕組分頁資料
        /* $SubtitlesPaginate = Animate_subtitles::OrderBy('created_at','desc')
            ->paginate($row_per_page); */
        
        //寫法1
        $Subtitles = Animate_subtitles::all()->sortBy('create_at');

        //寫法2
        // $Subtitles = Animate_subtitles::orderBy('created_at', 'desc')->get();
    
        $binding = [
            'title' => '後台字幕組列表',
            'SubtitlesPaginate' => $Subtitles,
        ];
    
        return view('ubold.subtitlesListPage' , $binding);
        // return "後台字幕組列表";
    }
}

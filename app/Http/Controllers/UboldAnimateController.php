<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Animate;
use App\Animate\Entity\Animate_subtitles;
use Validator;  // 驗證器
use Image;
use Illuminate\Support\Facades\Storage;

class UboldAnimateController extends Controller
{
    //動畫列表
    public function animateListPage()
    {

        $Animates = Animate::all()->sortBy('create_at');

        // 設定圖片網址
        foreach ($Animates as $Animate) {
            if (!is_null($Animate->pic_index)) {
                // 設定網址
                $Animate->pic_index = url($Animate->pic_index);
            }
        }
   
        $binding = [
            'title' => '後台動畫列表',
            'Animates' => $Animates,
        ];
 
        return view('ubold.animateListPage' , $binding);
        // return dd($Animates->toArray());
    }

    // 新增動畫
    public function animateCreateProcess(){
        
        /* $binding = [
            'title' => '新增動畫',
        ];
        return view('ubold.animateAddPage', $binding); */

        // 建立商品基本資訊
        $animate_data = [
            'name' => '',
            'show' => 0,
        ];
        $Animate = Animate::create($animate_data);
        
        // 重新導向至商品編輯頁
        return redirect('/ubold/animate/' . $Animate->id . '/edit');
    }

    // 動畫編輯頁
    public function animateItemEditPage($animate_id)
    {

        // 撈取動畫資料
        $Animate = Animate::findOrFail($animate_id);

        // 撈取字幕組資料
        $Animate_subtitles = Animate_subtitles::all();

        //作品類型
        $typeArray = array('科幻未來','奇幻冒險','推理懸疑','幽默搞笑','戀愛','靈異神怪','運動競技','溫馨','社會寫實','青春校園','料理美食','歷史傳記','其他');
        
        // return dd($Animate);

        if (!is_null($Animate->pic_recommend)) {
            // 設定首頁釘選圖片網址
            $Animate->pic_recommend = url($Animate->pic_recommend);
        }

        if (!is_null($Animate->pic_index)) {
            // 設定首頁圖片網址
            $Animate->pic_index = url($Animate->pic_index);
        }

        if (!is_null($Animate->pic_detail)) {
            // 設定介紹圖片網址
            $Animate->pic_detail = url($Animate->pic_detail);
        }
        
        $binding = [
            'title' => trans('動畫資料編輯'),
            'Animate'=> $Animate,
            'Animate_subtitles' => $Animate_subtitles,
            'typeArray' => $typeArray,
        ];
        return view('ubold.animateEditPage', $binding);
        // return $Animate_subtitles;
    }

    // 動畫資料更新處理
    public function animateItemUpdateProcess($animate_id)
    {

        // 撈取商品資料
        $Animate = Animate::findOrFail($animate_id);
        // 接收輸入資料
        $input = request()->all();
        
        $input["recommend"] = (isset($input["recommend"]))?"1":"0";   
        $input["show"] = (isset($input["show"]))?"1":"0";   
        $Animate->show = $input["show"];
        $input["shinban"] = (isset($input["shinban"]))?"1":"0"; 
        $Animate->shinban = $input["shinban"];
    
        // return dd($input);
    
        // 驗證規則
        $rules = [
            // 動畫名稱
            'name'=> [
                'required',
            ],
            // 商品名稱
            'name_jp' => [
                'required',
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/animate/' . $Animate->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        
        $path = 'uploads/animates/' . $Animate->id;
        if( !file_exists($path) ) {
            mkdir($path,0755);
        }

        //首頁釘選圖片
        if (isset($input['pic_recommend'])){
            // $input['pic_recommend'] = $input['pic_recommend'];
 
            // 有上傳圖片
            $pic_recommend = $input['pic_recommend'];
            // 檔案副檔名
            $file_extension = $pic_recommend->getClientOriginalExtension();
            // 產生自訂隨機檔案名稱
            $file_name = uniqid() . '.' . $file_extension;
            // 檔案相對路徑
            $file_relative_path = 'uploads/animates/' . $Animate->id .'/'. $file_name;
            // 檔案存放目錄為對外公開 public 目錄下的相對位置
            $file_path = public_path($file_relative_path);
            // 裁切圖片
            $image = Image::make($pic_recommend)->fit(380, 600)->save($file_path);
            // 設定圖片檔案相對位置
            $input['pic_recommend'] = $file_relative_path;

            //刪除原本的圖片
            $origin_path = public_path($Animate->pic_recommend);
            if ( $Animate->pic_recommend != null AND $Animate->pic_index != '' ) {
                unlink($origin_path);
                // exit('删除成功！');
            }

        } else {
            $input['pic_recommend'] = $Animate->pic_recommend;
        }

        //首頁圖片
        if (isset($input['pic_index'])){

            // 有上傳圖片
            $pic_index = $input['pic_index'];
            // 檔案副檔名
            $file_extension = $pic_index->getClientOriginalExtension();
            // 產生自訂隨機檔案名稱
            $file_name = uniqid() . '.' . $file_extension;
            // 檔案相對路徑
            $file_relative_path = 'uploads/animates/' . $Animate->id .'/'. $file_name;
            // 檔案存放目錄為對外公開 public 目錄下的相對位置
            $file_path = public_path($file_relative_path);
            // 裁切圖片
            $image = Image::make($pic_index)->fit(424, 424)->save($file_path);
            // 設定圖片檔案相對位置
            $input['pic_index'] = $file_relative_path;
  
            //刪除原本的圖片
            $origin_path = public_path($Animate->pic_index);
            if ( $Animate->pic_index != null AND $Animate->pic_index != '' ) {
                unlink($origin_path);
            }

        } else {
            $input['pic_index'] = $Animate->pic_index;
        }

        //介紹圖片
        if (isset($input['pic_detail'])){

            // 有上傳圖片
            $pic_detail = $input['pic_detail'];
            // 檔案副檔名
            $file_extension = $pic_detail->getClientOriginalExtension();
            // 產生自訂隨機檔案名稱
            $file_name = uniqid() . '.' . $file_extension;
            // 檔案相對路徑
            $file_relative_path = 'uploads/animates/' . $Animate->id .'/'. $file_name;
            // 檔案存放目錄為對外公開 public 目錄下的相對位置
            $file_path = public_path($file_relative_path);
            // 裁切圖片
            $image = Image::make($pic_detail)->fit(526, 773)->save($file_path);
            // 設定圖片檔案相對位置
            $input['pic_detail'] = $file_relative_path;

            //刪除原本的圖片
            $origin_path = public_path($Animate->pic_detail);
            if ( $Animate->pic_detail != null AND $Animate->pic_index != '' ) {
                unlink($origin_path);
            }

        } else {
            $input['pic_detail'] = $Animate->pic_detail;
        }
    
        // 動畫資料更新
        $Animate->update($input);

        // 重新導向到動畫編輯頁
        return redirect('/ubold/animate/' . $Animate->id . '/edit');

    }

    // 動畫資料處理-刪除
    public function animateDeleteProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $AnimateCheck = Animate::where('id', $input['id'])->count();
        if($AnimateCheck=='0') {
            return '找不到動畫';
        }

        // 撈取動畫資料
        $Animate = Animate::find($input['id']);
        // return dd($Animate->toArray());

        // 刪除動畫
        if( $Animate->delete($input['id']) ) {

            //設定需要刪除的資料夾
            $path = 'uploads/animates/' . $Animate->id;
            if(file_exists($path)){
                // return '資料夾存在';
                $pic_recommend_path = public_path($Animate->pic_recommend);
                $pic_index_path     = public_path($Animate->pic_index);
                $pic_detail_path    = public_path($Animate->pic_detail);

                // return $pic_recommend_path.','.$pic_index_path.','.$pic_detail_path;

                if(is_file($pic_recommend_path)){
                    unlink($pic_recommend_path);  
                }

                if(is_file($pic_index_path)){
                    unlink($pic_index_path);  
                }

                if(is_file($pic_detail_path)){
                    unlink($pic_detail_path);  
                }

                if(!rmdir($path)){
                    return '002';
                }
            } else { 
                return '002'; 
            }

            return '000';
        } else {
            return '001';
        };

        
        
        return '001';

    }
}

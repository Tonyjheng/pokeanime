<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animate\Entity\Blog;
use Validator;  // 驗證器
use Image;

class UboldBlogController extends Controller
{
    //文章列表
    public function blogListPage()
    { 

        $Blogs = Blog::all()->where('seal', '!=', 1)->sortBy('create_at');

        // 設定圖片網址
        foreach ($Blogs as $Blog) {
            if (!is_null($Blog->cover)) {
                // 設定網址
                $Blog->cover = url($Blog->cover);
            }
            if (!is_null($Blog->cover_index)) {
                // 設定網址
                $Blog->cover_index = url($Blog->cover_index);
            }
        }
   
        $binding = [
            'title' => '後台文章列表',
            'Blogs' => $Blogs,
        ];
 
        return view('ubold.blogListPage' , $binding);
        // return dd($Animates->toArray());
    }

    // 新增文章資料
    public function blogAddProcess(){
        $postDate = date('Y-m-d H:i:s');
        
        // 建立文章基本資訊
        $blog_data = [
            'postDate' => $postDate,
            'show' => 0,
        ];
        $Blog = Blog::create($blog_data);

        // 重新導向至文章編輯頁
        return redirect('/ubold/blog/' . $Blog->id . '/edit');
    }

    // 文章編輯頁
    public function blogItemEditPage($blog_id)
    {
        // 撈取文章資料
        $Blog = Blog::findOrFail($blog_id);
        // return dd($Blog->toArray());

        if (!is_null($Blog->cover)) {
            // 設定圖片網址
            $Blog->cover = url($Blog->cover);
        }

        if (!is_null($Blog->cover_index)) {
            // 設定圖片網址
            $Blog->cover_index = url($Blog->cover_index);
        }
        
        $binding = [
            'title' => '後台文章編輯',
            'Blog'=> $Blog,
        ];
        return view('ubold.blogEditPage', $binding);
        // return $Animate_subtitles;
    }

    // 文章資料更新處理
    public function blogItemUpdateProcess($blog_id)
    {
        // 撈取文章資料
        $Blog = Blog::findOrFail($blog_id);
        // 接收輸入資料
        $input = request()->all();
        $input["show"] = (isset($input["show"]))?"1":"0";   

        // return dd($input);
        // return dd($Blog);
    
        // 驗證規則
        $rules = [
            // 標題
            'title'=> [
                'required',
            ],
        ];
    
        // 驗證資料
        $validator = Validator::make($input, $rules);
    
        if ($validator->fails()) {
            // 資料驗證錯誤
            return redirect('/ubold/blog/' . $Blog->id . '/edit')
                ->withErrors($validator)
                ->withInput();
        }
        
        $path = 'uploads/blogs/' . $Blog->id;
        if( !file_exists($path) ) {
            mkdir($path,0755);
        }

        //封面圖片
        if (isset($input['cover'])){
            // 有上傳圖片
            $pic_recommend = $input['cover'];
            // 檔案副檔名
            $file_extension = $pic_recommend->getClientOriginalExtension();
            // 產生自訂隨機檔案名稱
            $file_name = uniqid() . '.' . $file_extension;
            // 檔案相對路徑
            $file_relative_path = 'uploads/blogs/' . $Blog->id .'/'. $file_name;
            // 檔案存放目錄為對外公開 public 目錄下的相對位置
            $file_path = public_path($file_relative_path);
            // 裁切圖片
            $image = Image::make($pic_recommend)->fit(1600, 600)->save($file_path);
            // 設定圖片檔案相對位置
            $input['cover'] = $file_relative_path;

            //刪除原本的圖片
            $origin_path = public_path($Blog->cover);
            if ( $Blog->cover != null AND $Blog->cover != '' ) {
                unlink($origin_path);
            }

        } else {
            $input['cover'] = $Blog->cover;
        }

        //首頁封面圖片
        if (isset($input['cover_index'])){
            // 有上傳圖片
            $cover_index = $input['cover_index'];
            // 檔案副檔名
            $file_extension = $cover_index->getClientOriginalExtension();
            // 產生自訂隨機檔案名稱
            $file_name = uniqid() . '.' . $file_extension;
            // 檔案相對路徑
            $file_relative_path = 'uploads/blogs/' . $Blog->id .'/'. $file_name;
            // 檔案存放目錄為對外公開 public 目錄下的相對位置
            $file_path = public_path($file_relative_path);
            // 裁切圖片
            $image = Image::make($cover_index)->fit(270, 330)->save($file_path);
            // 設定圖片檔案相對位置
            $input['cover_index'] = $file_relative_path;

            //刪除原本的圖片
            $origin_path = public_path($Blog->cover_index);
            if ( $Blog->cover_index != null AND $Blog->cover_index != '' ) {
                unlink($origin_path);
            }

        } else {
            $input['cover_index'] = $Blog->cover_index;
        }

        // dd($input);
    
        // 資料更新
        $Blog->update($input);

        // 重新導向到編輯頁
        return redirect('/ubold/blog/' . $Blog->id . '/edit');

    }

    // 文章資料處理-刪除
    public function blogDeleteProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $BlogCheck = Blog::where('id', $input['id'])->count();
        if($BlogCheck=='0') {
            return '找不到文章';
        }

        // 撈取文章資料
        $Blog = Blog::findOrFail($input['id']);
        // return dd($Blog->toArray());

        $path = 'uploads/blogs/' . $Blog->id;
        //掃描一個資料夾內的所有資料夾和檔案並返回陣列
        $p = scandir($path);
        
        foreach ($p as $value) {
            $delpath = public_path($path.'/'.$value);
            if($value != '.' && $value != '..') {
                unlink($delpath);
            }
        }

        if(!rmdir($path)){
            return '002';
        }

        // 刪除動畫
        if( $Blog->delete($input['id']) ) {
            return '000';
        } else {
            return '001';
        };
        
        return '001';

    }

    // 文章資料處理-封存
    public function blogSealProcess()
    {
        $input = request()->all();

        //確認是否有該筆資料
        $BlogCheck = Blog::where('id', $input['id'])->count();
        if($BlogCheck=='0') {
            return '找不到文章';
        }

        // 撈取文章資料
        $Blog = Blog::findOrFail($input['id']);
        // return dd($Blog->toArray());

        $Blog->seal = '1';

        // 封存文章
        if( $Blog->save() ) {
            return '000';
        } else {
            return '001';
        };
        
        return '001';

    }

}

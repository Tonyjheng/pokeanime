POKEANIME
-------------------------------------

這是一個給動畫做評分的網站

同事或朋友時常會問我有沒有什麼好看的動畫

剛好就有在寫網頁

聯想到電影有IMDB那動畫呢?

於是就想寫一個評分動畫的網站來分享資訊

這個網站就是以評分為出發點開始延伸功能

本網站有許多方便的功能

像是篩選動畫類型、關鍵字搜尋(可搜尋依:作者、導演監督、製作廠商、聲優、台灣代理)

讓您可以快速找到自己喜歡的作品

# 畫面預覽

- ## 首頁
![圖片](https://images2.imgbox.com/8d/80/5QPVyDz1_o.jpg)

- ## 動畫詳細頁面
![圖片](https://images2.imgbox.com/5d/01/qKZGjHPH_o.jpg)

- ## 動畫排行
![圖片](https://images2.imgbox.com/71/7a/XyRCI37a_o.jpg)

- ## 動畫列表
![圖片](https://images2.imgbox.com/e1/c1/pukDiZ3D_o.jpg)

- ## 文章
![圖片](https://images2.imgbox.com/8b/e2/2wQ686iL_o.jpg)

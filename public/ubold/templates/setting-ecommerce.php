<div class="btn-group pull-right m-t-15">
    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">設定 <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
    <ul class="dropdown-menu drop-menu-right" role="menu">
        <li><a href="product-add.php">新增商品</a></li>
        <li><a href="category-add.php">新增類別</a></li>
        <li><a href="categorySub-add.php">新增子類別</a></li>
        <li><a href="categoryMenu-add.php">新增選單類別</a></li>
        <li><a href="product_size-add.php">新增商品規格</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>
    </ul>
</div>
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//首頁動畫
/* Route::get('/', function () {
    return view('index');
}); */
Route::get('/','AnimateController@index');


//文章
Route::group(['prefix' => 'blog'], function(){

    //文章列表
    Route::get('/', 'BlogController@blogListPage');

    //搜尋頁
    Route::get('/search', 'BlogController@blogSearchPage');

    //指定
    Route::group(['prefix' => '{blog_id}'], function(){
        Route::get('/', 'BlogController@blogDetailPage');
    });
});


//動畫
Route::group(['prefix' => 'animate'], function(){

    //新番列表
    Route::get('/list', 'AnimateController@animateListPage');

    //新番排行
    Route::get('/rate', 'AnimateController@animateRateListPage');

    //已播映列表
    Route::get('/oldlist', 'AnimateController@animateoldListPage');

    //已播映排行
    Route::get('/oldrate', 'AnimateController@animateoldRateListPage');

    //舊番列表-特色分類篩選
    Route::get('/oldlist/{type}', 'AnimateController@animateoldTypeListPage');

    //動畫搜尋結果頁
    Route::post('/search', 'AnimateController@animateSearchPage');


    Route::group(['prefix' => '{animate_id}'], function(){
        Route::get('/', 'AnimateController@animateDetailPage');
    });
});


Route::get('/test', 'testController@create');

Route::get('/page-starter', function () {
    return view('ubold/page-starter');
});

// 後台區
Route::group(['prefix' => 'ubold'], function () {

        Route::group(['middleware' => ['user.auth.admin']], function(){
            //字幕組清單檢視
            Route::get('/subtitles','UboldSubtitlesController@subtitlesListPage');
            //新增字幕組頁面
            Route::get('/subtitlesAdd', 'UboldSubtitlesController@subtitlesAddPage');
            //新增字幕組資料處理
            Route::post('/subtitlesAdd', 'UboldSubtitlesController@subtitlesAddProcess');
            //刪除字幕組
            Route::delete('/subtitlesdel', 'UboldSubtitlesController@subtitlesDeleteProcess');

            // 指定字幕組
            Route::group(['prefix' => '{subtitles_id}'], function(){
                //字幕組修改畫面
                Route::get('/edit', 'UboldSubtitlesController@subtitlesEditPage');
                //處理字幕組資料
                Route::put('/', 'UboldSubtitlesController@subtitlesUpdateProcess');
            });
        });

        //動畫
        Route::group(['prefix' => 'animate'], function(){
            Route::group(['middleware' => ['user.auth.admin']], function(){
                //動畫清單檢視
                Route::get('/','UboldAnimateController@animateListPage');
                // 新增動畫
                Route::get('/animateAdd', 'UboldAnimateController@animateCreateProcess');
                //刪除動畫
                Route::delete('/animateDel', 'UboldAnimateController@animateDeleteProcess');

                Route::group(['prefix' => '{animate_id}'], function(){
                    Route::get('/edit', 'UboldAnimateController@animateItemEditPage');
                    Route::put('/', 'UboldAnimateController@animateItemUpdateProcess');
                });
            });
        });

        //部落格
        Route::group(['prefix' => 'blog'], function(){
            Route::group(['middleware' => ['user.auth.admin']], function(){
                //文章清單檢視
                Route::get('/','UboldBlogController@blogListPage');
                //新增文章資料處理
                Route::get('/blogAdd', 'UboldBlogController@blogAddProcess');
                //刪除文章
                Route::delete('/blogDel', 'UboldBlogController@blogDeleteProcess');
                //封存文章
                Route::delete('/blogSeal', 'UboldBlogController@blogSealProcess');

                Route::group(['prefix' => '{blog_id}'], function(){
                    Route::get('/edit', 'UboldblogController@blogItemEditPage');
                    Route::put('/', 'UboldblogController@blogItemUpdateProcess');
                });
            });
        });

        //使用者
        Route::group(['prefix' => 'users'], function(){
            Route::group(['middleware' => ['user.auth.admin']], function(){
                //使用者清單檢視
                Route::get('/','UboldUsersController@userListPage');
                //新增會員頁面
                Route::get('/userAdd', 'UboldUsersController@userAddPage');
                //新增會員資料處理
                Route::post('/userAdd', 'UboldUsersController@userAddProcess');
                //刪除會員
                Route::delete('/userDel', 'UboldUsersController@userDeleteProcess');
                //封存會員
                Route::delete('/userSeal', 'UboldUsersController@userSealProcess');
                //取消封存會員
                Route::post('/userSealCancel', 'UboldUsersController@userSealCancelProcess');

                // 指定會員
                Route::group(['prefix' => '{user_id}'], function(){
                    //會員資料修改畫面
                    Route::get('/edit', 'UboldUsersController@userEditPage');
                    //處理會員資料
                    Route::put('/', 'UboldUsersController@userUpdateProcess');
                });
            });
        });

        //登入畫面
        Route::get('/login','UboldUsersController@loginPage');
        //登入會員資料驗證
        Route::post('/login','UboldUsersController@loginProcess');
        //會員登出
        Route::get('/logout','UboldUsersController@logoutProcess');

        //後台ckeditor使用
        Route::get('/elfinder', function () {
            return view('ubold.elfinder');
        })->middleware(['user.auth.admin']);
});
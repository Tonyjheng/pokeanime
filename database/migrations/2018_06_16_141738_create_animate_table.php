<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnimateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animate', function (Blueprint $table) {
            //id自動遞增
            $table->increments('id');
            //動畫狀態
            // Play   - 連載中
            // Finish - 已完結
            $table->string('status', 40)->default('P')->comment('動畫狀態');
            //動畫名稱
            $table->string('name', 80)->comment('動畫名稱');            
            //動畫日文名稱
            $table->string('name_jp', 80)->nullable()->comment('動畫日文名稱');            
            //觀看狀態
            // Read   - 觀看中
            // Finish - 已看完  
            // Giveup - 棄番
            $table->string('read', 40)->default('R')->comment('觀看狀態');            
            //動畫簡介
            $table->text('introduction')->nullable()->comment('動畫簡介');      
            //動畫短評
            $table->text('myreview')->nullable()->comment('動畫短評');     
            //我的評價 
            $table->integer('point')->nullable()->comment('我的評價');     
            //作品類型
            $table->string('type', 50)->nullable()->comment('作品類型');     
            //播映方式
            $table->string('mode', 50)->nullable()->comment('播映方式');     
            //當地首播
            $table->date('premiere')->nullable()->comment('當地首播');
            //播出集數        
            $table->integer('episodes')->nullable()->comment('播出集數');
            //原著作者     
            $table->string('author', 50)->nullable()->comment('原著作者');
            //導演監督
            $table->string('director', 50)->nullable()->comment('導演監督');
            //製作廠商
            $table->string('company', 50)->nullable()->comment('製作廠商');
            //台灣代理
            $table->string('proxy', 50)->nullable()->comment('台灣代理');
            //官方網站
            $table->string('website', 255)->nullable()->comment('官方網站');
            //動畫PV
            $table->string('pv', 255)->nullable()->comment('動畫PV');
            //檔案大小
            $table->string('filesize', 50)->nullable()->comment('檔案大小'); 
            //字幕組
            $table->string('subtitle', 50)->nullable()->comment('字幕組');
            //存放位置         
            $table->string('storage', 50)->nullable()->comment('存放位置');
            //動畫分級
            $table->string('level', 50)->nullable()->comment('動畫分級');
            //聲優
            $table->text('cast')->nullable()->comment('聲優');
            //製作團隊
            $table->text('staff')->nullable()->comment('製作團隊');
            //巴哈姆特動畫瘋
            $table->string('gamer', 255)->nullable()->comment('巴哈姆特動畫瘋');
            //bilibili
            $table->string('bilibili', 255)->nullable()->comment('bilibili');
            //iqiyi
            $table->string('iqiyi', 255)->nullable()->comment('iqiyi');
            //首頁釘選
            $table->integer('recommend')->nullable()->default(0)->comment('首頁釘選');
            //是否為新番
            $table->integer('shinban')->nullable()->default(0)->comment('是否為新番');
            //首頁釘選圖片
            $table->string('pic_recommend', 255)->nullable()->comment('首頁釘選圖片');
            //首頁圖片           
            $table->string('pic_index', 255)->nullable()->comment('首頁圖片');
            //介紹圖片
            $table->string('pic_detail', 255)->nullable()->comment('介紹圖片');
            //備註
            $table->text('remark')->nullable()->comment('備註');
            //點擊數
            $table->integer('hits')->nullable()->comment('點擊數');
            //排序         
            $table->integer('sort')->nullable()->comment('排序');
            //是否顯示
            // 0 - 隱藏
            // 1 - 顯示
            $table->integer('show')->default(0)->comment('是否顯示');
            //時間戳記
            $table->timestamps();

            // 索引設定
            $table->index(['name'], 'animate_name_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animate');
    }
}

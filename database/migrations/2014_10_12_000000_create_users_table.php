<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 建立資料表
        Schema::create('users', function (Blueprint $table) {
            // 會員編號
            $table->increments('id');
            // 姓名
            $table->string('name', 50)->comment('姓名');
            // 暱稱
            $table->string('nickname', 50)->nullable()->comment('暱稱');
            // Email
            $table->string('email', 150)->unique()->nullable()->comment('Email');
            // 密碼
            $table->string('password', 60)->comment('密碼');
            // 帳號類型（type）:用於識別登入會員身份
            // - A（Admin）  : 管理者
            // - G（General）: 一般會員
            $table->string('type', 1)->default('G')->comment('帳號類型');
            //是否封存
            // 0 - 否
            // 1 - 是
            $table->integer('seal')->default(0)->comment('是否封存');
            //登入權限
            // 0 - 正常
            // 1 - 禁止登入
            $table->integer('status')->default(0)->comment('登入權限');
            // Token
            // $table->rememberToken();
            $table->string('remember_token')->comment('Token');
            //備註
            $table->text('remark')->nullable()->comment('備註');
            //時間戳記
            $table->timestamps();

            // 鍵值
            $table->unique(['id'], 'user_id_pk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog', function (Blueprint $table) {
            //id自動遞增
            $table->increments('id');
            //文章標題
            $table->string('title', 150)->nullable()->comment('文章標題');
            //發佈日期
            $table->dateTime('postDate')->nullable()->comment('發佈日期');
            //列表簡介
            $table->text('listIntro')->nullable()->comment('列表簡介');
            //文章內文
            $table->text('content')->nullable()->comment('文章內文');
            //文章封面圖
            $table->string('cover', 255)->nullable()->comment('文章封面圖');
            //文章首頁封面圖
            $table->string('cover_index', 255)->nullable()->comment('文章首頁封面圖');
            //備註
            $table->text('remark')->nullable()->comment('備註');
            //點擊數
            $table->integer('hits')->nullable()->comment('點擊數');
            //排序         
            $table->integer('sort')->nullable()->comment('排序');
            //是否封存
            // 0 - 否
            // 1 - 是
            $table->integer('seal')->default(0)->comment('是否封存');
            //是否顯示
            // 0 - 隱藏
            // 1 - 顯示
            $table->integer('show')->default(0)->comment('是否顯示');

            //時間戳記
            $table->timestamps();

            // 索引設定
            $table->index(['id'], 'blog_id_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnimateSubtitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animate_subtitles', function (Blueprint $table) {
            //id自動遞增
            $table->increments('id');
            //字幕組名稱
            $table->string('subtitles_name')->comment('字幕組名稱');
            //排序
            $table->integer('sort')->nullable()->comment('排序');
            //備註
            $table->text('remark')->nullable()->comment('備註');
            //時間戳記
            $table->timestamps();

            // 索引設定
            $table->index(['subtitles_name'], 'subtitles_name_idx');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animate_subtitles');
    }
}
